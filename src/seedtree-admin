#!/bin/bash
#
# Command line utility to manage filetree/seedtree databases
#
# see nice tips on
# http://www.manniwood.com/postgresql_and_bash_stuff/index.html

# fail on uninitialized vars rather than treating them as null
set -u
# fail on the first program that returns $? != 0
set -e

# configuration script (use the first one found in path)
CONFIG_SCRIPT="seedtree_config.py"

# check if config script is in the path
which $CONFIG_SCRIPT > /dev/null

# get configuration settings
export PGHOST=`$CONFIG_SCRIPT | grep DB_HOST | cut -f 2`
export PGDATABASE=`$CONFIG_SCRIPT | grep DB_NAME | cut -f 2`
export PGUSER=`$CONFIG_SCRIPT | grep DB_USER | cut -f 2`
export PGPASSWORD=`$CONFIG_SCRIPT | grep DB_PASSWORD | cut -f 2`

PSQL_OPTIONS="--no-psqlrc --echo-all --single-transaction --set AUTOCOMMIT=off --set ON_ERROR_STOP=on"

#
# display help text
#
echousage() {
    bn=`basename $0`
    cat > /dev/stderr << EOF
$bn is a tool for managing Filetree/Seedtree databases.

CREATE A NEW SCHEMA
    $bn --create <schema name> "your comment on schema"

DROP A SCHEMA
    $bn --drop <schema name>

LIST ALL SCHEMAS
    $bn --list 

CONFIGURATION FILE
    $CONFIG_SCRIPT

CAVEATS

Seedtree uses database schemas to group scans logically. Schema names must fullfill SQL naming standards.  
The default 'public' schema should not be used. See Postgres documentation for more information.

Configuration file search path depends on both \$PATH and \$PYTHONPATH environnement variables : run "which $CONFIG_SCRIPT" 
to check which config file will be used. If needed, alter both variables.

EOF

    exit 1
}

#
#  echo to stderr
#
echoerr() { echo "$@" 1>&2; }

# MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

[ "$#" -lt 1 ] && echousage

# check how we were called
case $1 in
    "--create")
        [ "$#" -ne 3 ] && echousage
        [ $2 == "public" ] && echoerr "Invalid schema." && exit 1 
        sql=`dirname $0`/../sql/schema_init.sql
        psql $PSQL_OPTIONS --set newschema=$2 --set commentschema="'$3'" -f $sql
        echoerr OK        
        ;;
    "--drop")        
        [ "$#" -ne 2 ] && echousage
        [ $2 == "public" ] && echoerr "Invalid schema." && exit 1 
        psql $PSQL_OPTIONS --command="DROP SCHEMA $2 CASCADE; COMMIT;"
        echoerr OK 
        ;;
    "--list")        
        psql $PSQL_OPTIONS --command="\dn+"  | sed '/standard public schema/d' | sed '/information_schema/d' | sed '/pg_/d' | sed '/postgres/d' | sed '/dn+/d'
        ;;
    *)
        echousage
        ;;
esac

