#!/bin/bash

# Seedtree scan "partitionner" ---------------
#
# This scripts takes filenames on stdin, one per line. 
#
# The script prints on stdout a serie of seedtree scan commands. 
# Each scan embeds a subset (aka. partition) of the provided input files. 
# Partitions are built with fpart utility (see fpart manpage for details).
#
# This script is useful to perform incremental scans of large set of datas,
# and may be used when doing a "initial first scan" of such large sets.
#
# typical use:
#
# find /my/data -type f | fpart_scan.sh <number of files in partition> <schema name> <timeout> (see timeout command)>
#
# Example:
# 

set -e

# check fpart is in the path
command -v fpart >/dev/null || { echo "fpart command not found." >&2; exit 1; }

# check timout is in the path
command -v timeout >/dev/null || { echo "timeout command not found." >&2; exit 1; }

# pick a random number
random=`perl -e 'print int rand 99999, "\n"; '`

# check command line arguments
filesperpartition=$1
[[ ! $filesperpartition =~ ^-?[0-9]+$ ]] && echo "Not a valid number" && exit 1;

# set Internal Field Separator to newline character
IFS=$'\n'

# send stdin to fpart, get partitions filenames in an array
partitions=($(cat | fpart -L -f $filesperpartition -o /tmp/fpart-$RANDOM -W 'echo ${FPART_PARTFILENAME}' -i -))

# print seedtree commands on stdout

for partfile in "${partitions[@]}"
do
    echo "date"
    echo Scanning $partfile
    echo "cat $partfile | timeout -k 5 $3 filetree.py --insert $2"
    echo "seedtree5.py --sync $2"
done

echo \# You may run "rm -f /tmp/fpart-$random*" after usage. >&2

