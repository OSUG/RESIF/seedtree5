#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Python standard library
import argparse
import logging
import multiprocessing
import os
import random
import string
import time
import traceback

from collections import namedtuple
from collections import OrderedDict
from cStringIO import StringIO

# https://pypi.python.org/pypi/tabulate
from tabulate import tabulate

# https://pypi.python.org/pypi/asciitree
from asciitree import LeftAligned

# homemade modules
from filetree import Filetree
import readmseed
import seedtree_config
import myTools


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Seedtree:

    SCRIPT_VERSION = 2017.166

    __SQL_TRACES_COLUMNS = ('fileindex', 'network', 'station', 'location', 'channel', 'quality',
    'starttime', 'endtime', 'nsamples', 'samplerate', 'recordlength', 'fileoffset')

    __SQL_INSERT_REJECTED_FILE = '''INSERT INTO rejectedminiseedfiles(fileindex,comment) VALUES (%s,%s) ON CONFLICT (fileindex) DO NOTHING'''

    __SQL_RESET_GAPS = '''reset_gaps'''
    __SQL_GAPS_COLUMNS = ( 'network', 'station', 'location', 'channel', 
        'previous_traceindex', 'current_traceindex', 'previous_endtime', 'current_starttime',
        'previous_dirname', 'previous_basename', 'current_dirname', 'current_basename', 
        'previous_quality', 'current_quality', 'expecteddelta', 'truedelta', 'ratiodelta' )    
    __SQL_CREATE_INDEX_GAPS = '''create_index_on_gaps'''
    __SQL_GET_QUALITIES_AND_SAMPLES = '''get_qualities_and_samples'''
    __SQL_GET_REJECTED_FILES = '''SELECT CONCAT(files.dirname,'/',files.basename) AS rejected, comment FROM files,rejectedminiseedfiles WHERE files.fileindex=rejectedminiseedfiles.fileindex'''

    __SQL_DATAAVAILABILITY_COLUMNS = ( 'network', 'station', 'location', 'channel', 'starttime', 'endtime', 'type' )

    # FIXME should use its own python API instead of doing SQL selects.
    __SQL_SELECT_TRACES = '''SELECT files.fileindex,traces.traceindex,dirname,basename,network,station,location,channel,quality,samplerate,
    nsamples,starttime,endtime FROM traces,files WHERE traces.fileindex=files.fileindex AND 
    network=%s AND station=%s AND location=%s AND channel=%s AND quality=%s AND (starttime,endtime) OVERLAPS (%s,%s) ORDER BY starttime,endtime'''
    __SQL_SELECT_TRACES2 = '''SELECT * FROM traces WHERE network=%s AND station=%s AND location=%s AND channel=%s AND 
    ( EXTRACT(year FROM starttime)=%s AND EXTRACT(year FROM endtime)=%s ) ORDER BY starttime,endtime'''
    __SQL_SELECT_TRACES3 = '''SELECT files.fileindex,traces.traceindex,dirname,basename,network,station,location,channel,quality,samplerate,
    nsamples,starttime,endtime FROM traces,files WHERE traces.fileindex=files.fileindex AND network=%s AND station=%s AND location=%s 
    AND channel=%s AND quality=%s AND ( EXTRACT(year FROM starttime)=%s or EXTRACT(year FROM endtime)=%s ) ORDER BY starttime,endtime'''

    # database parameters
    myconnection = seedtree_config.DB_CONNECTION
    # seedtree_config.set_autocommit_mode(myconnection) 
    mycursor = seedtree_config.DB_CURSOR
    myschema = None
    mynamedcursor = None
    
    # logging
    logger = None

    # filelist is processed by chunks, containing such number of files
    # timeout calcultation is made  empirically from this value. (see code below)
    FILELIST_CHUNK_SIZE = seedtree_config.FILELIST_CHUNK_SIZE
    # read timeout in seconds, for the whole chunk (see code below)
    READ_TIMEOUT_PER_CHUNK = seedtree_config.READ_TIMEOUT_PER_CHUNK

    # API functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_networks ( self ) :
        """get available networks"""
        self.mycursor.callproc ( "get_networks" )
        networks = self.mycursor.fetchall()
        return [n[0] for n in networks]
        
    def get_stations ( self, network ) :
        """get available stations for this network"""
        self.mycursor.callproc ( "get_stations", [ network ] )
        stations = self.mycursor.fetchall()
        return [s[0] for s in stations]
                
    def get_location_codes ( self, network, station ) :
        """get a station's location codes"""
        self.mycursor.callproc ( "get_location_codes", [ network,  station ] )
        locations = self.mycursor.fetchall()
        return [l[0] for l in locations]

    def get_stations_details ( self, network ) :
        """get available stations for this network, with whole details"""
        self.mycursor.callproc ( "get_stations_details", [ network ] )
        return self.mycursor.fetchall()
        
    def get_channels ( self, network, station, location ) :
        """get channels for this network.station.location code"""
        self.mycursor.callproc ( "get_channels", [ network,  station, location ] )
        channels = self.mycursor.fetchall()
        return [c[0] for c in channels]
        
    def get_all_channels ( self ) :
        """get all available channels"""
        self.mycursor.callproc ( "get_all_channels" )
        return self.mycursor.fetchall()
    
    def get_qualities_and_samples ( self, network, station, location, channel ) :
        """get available qualities and samples for a channel"""
        self.mycursor.callproc ( "get_qualities_and_samples", [ network,  station, location, channel ] )
        return self.mycursor.fetchall()

    def get_updated_channels ( self, style = 'without_quality' ) :
        """get channels updated since last scan, with timespan"""
        if style == 'with_quality' : self.mycursor.callproc ( "get_updated_channels_including_quality" )
        if style == 'without_quality': self.mycursor.callproc ( "get_updated_channels" )
        if style == 'without_timestamps': self.mycursor.callproc ( "get_updated_channels_without_timestamps" )
        return self.mycursor.fetchall()
                
    def open_traces ( self, network=None, station=None, location=None, channel=None, starttime=None, endtime=None, samplerate=None, quality=None, recordlength=None ):
        """get traces according to optional search parameters ('None' means any).
        Returns a cursor name that may be used at will.
        It is also possible to use iteration on class instance to get traces one by one"""
        t = [ network, station, location, channel, starttime, endtime, samplerate, quality, recordlength ]
        self.mycursor.callproc ( "open_traces", t )
        # get cursor name generated on server side
        name = self.mycursor.fetchone()
        # create a psycopg cursor object from this server-side cursor,
        self.mynamedcursor = seedtree_config.get_database_server_cursor(self.myconnection, cursorname = name[0])
        # returns cursor name, 
        return ( name[0] )

    def get_dataavailability ( self, network=None, station=None, location=None, channel=None, starttime=None, endtime=None, gaps = False ):
        """
        Returns data availability for a channel. Every parameter is optional.
        If gaps=True, then data un-availability is returned instead. 
        """
        t = [ network, station, location, channel, starttime, endtime, gaps ]
        self.mycursor.callproc ( 'get_dataavailability', t )
        return self.mycursor.fetchall()
         
    def close_traces ( self ):
        """close current named cursor"""
        if self.mynamedcursor:
            self.mynamedcursor.close()
            self.mynamedcursor = None
        return None    

    def next ( self ):
        """returns next item for current named cursor - eg: see open_traces()"""
        row = self.mynamedcursor.fetchone()
        if row: return row 
        # if no more rows : close named cursor and stop iteration
        self.mynamedcursor.close()
        self.mynamedcursor = None
        raise StopIteration

    def get_gaps ( self, network=None, station=None, location=None, channel=None, minratio=None, maxratio=None, startdate=None, enddate=None, quality=None):
      """get gaps or overlaps according to search parameters""" 
      t = [ network, station, location, channel, minratio, maxratio, startdate, enddate, quality ]
      self.mycursor.callproc ( '''get_gaps''', t )  
      gaps = self.mycursor.fetchall()
      return gaps

    def get_channel_mintime( self, network, station, location, channel, startdate = None):
      """returns start time of the very first trace for this channel (after optional date)"""
      t = [ network, station, location, channel, startdate ]
      self.mycursor.callproc ( 'get_channel_mintime', t )
      return self.mycursor.fetchone()[0]

    def get_channel_maxtime( self, network, station, location, channel, enddate = None):
      """returns end time of the very last trace for this channel (before optional date)"""
      t = [ network, station, location, channel, enddate ]
      self.mycursor.callproc ( 'get_channel_maxtime', t )
      return self.mycursor.fetchone()[0]
    
    def get_last_scan_date ( self ):
      """returns the most recent date a scan was made"""
      self.mycursor.callproc ( 'get_last_scan_date' )
      return self.mycursor.fetchone()[0]
          
    # END OF API ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def sync ( self, ignoretimeout = False ):
        """
        get journal from Filetree, 
        process any miniseed file that need to be inserted or deleted 
        """
        myfiletree = Filetree ( schema = self.myschema )
        journal = myfiletree.get_journal_without_duplicates()
        toBeDeleted = [ (j.fileindex, j.dirname, j.basename) for j in journal if j.event=='delete' or j.event=='update' ] 
        toBeInserted = [ (j.fileindex, j.dirname, j.basename) for j in journal if j.event=='create' or j.event=='update' ]        
        if toBeDeleted: self.delete_files ( toBeDeleted )
        if toBeInserted: self.insert_files ( toBeInserted, ignoretimeout )
        myfiletree.empty_journal()
        self.logger.info("committing to database...") 
        self.myconnection.commit()
        return None
               
    def flush_to_database ( self, bulkinserts ):
        """
        flush miniseed traces from memory to database
        """
        self.logger.info("flushing miniseed traces to database...") 
        bulkinserts.flush()
        bulkinserts.seek(0)
        self.mycursor.copy_from ( bulkinserts, 'traces', columns = self.__SQL_TRACES_COLUMNS )
        bulkinserts.close()
        self.logger.info("done flushing.") 
        return StringIO()
    
    def delete_files ( self, filelist ):
        """
        delete all miniseed traces contained in each file of filelist
        """
        indexesString = '(' + ','.join(str(x[0]) for x in filelist) + ')'
        self.logger.debug ("deleting traces for file %s" % ','.join((str(x[0]) for x in filelist)))
        self.mycursor.execute ('DELETE FROM traces WHERE fileindex IN ' + indexesString)
        self.mycursor.execute ('DELETE FROM rejectedminiseedfiles WHERE fileindex IN ' + indexesString)
        return None
            
    def insert_files ( self, filelist, ignoretimeout ):
        """
        read all miniseed files contained in filelist,
        compute miniseed traces, insert into database
        """
        # string for bulk inserts (SQL "copy from") 
        allbulkinserts = StringIO()
        donefiles = 0
        firstinsert = True
        dropindex = len(filelist) > 30000
        
        # drop indexes for fast populate
        if dropindex:
            self.logger.info("Dropping Traces index")
            self.mycursor.callproc('''drop_index_on_traces''')
            # have to commit because dropping indexes holds exclusive lock on whole table
            # FIXME if exit/failure happens, we may end up with no coherency in db
            # FIXME use savepoint ? http://www.postgresql.org/docs/9.3/static/sql-savepoint.html
            self.logger.info("Comitting changes...")
            self.myconnection.commit()

        # create pool of processes
        self.logger.info("Launching process pool...")
        pool = multiprocessing.Pool ( processes = seedtree_config.MINISEED_WORKERS )
        start = time.time()

        # split filelist in chunks, lauch readmseed asynchronously on each files 
        for fchunk in myTools.grouper ( filelist, self.FILELIST_CHUNK_SIZE ):
            # (skip trailing chunks that are 'None' : see 'grouper' function details)
            # FIXME an exception raised by one instance of msi2bulktraces() or get() will not be catched.
            # FIXME for unknown reason, the readmseed.msi2bulktraces() sometimes timeout , 
            # blocking indefinitely the whole loop. A timeout is set to avoid such blocking.
            results = []
            self.logger.debug("Reading {0}".format(fchunk))
            results = [ pool.apply_async (readmseed.msi2bulktraces, args=(s,)) for s in fchunk if s ]
            try:
                bulktraces = [ p.get(timeout = self.READ_TIMEOUT_PER_CHUNK if not ignoretimeout else None) for p in results ]
            except multiprocessing.TimeoutError as e:
                for f in fchunk:
                    if f:
                        self.logger.error ("giving up on chunk processing. File = %s (timeout or subprocess error)" % os.path.join(f[1],f[2]))
                        self.mycursor.execute ( Seedtree.__SQL_INSERT_REJECTED_FILE, [ f[0], 'processing or timeout error'] )
                continue
            # we now have a stringIO for each parsed miniseed file :
            # append each stringIO to a single big stringIO,
            # or reject miniseed file if too many traces  
            for (fileindex, filepath, nbtraces, bulktrace) in bulktraces:
                self.logger.debug ( "%d traces in %s" % (nbtraces, filepath) )
                if nbtraces <= seedtree_config.MINISEED_MAX_TRACES_PER_FILE :
                    allbulkinserts.write('\n' if not firstinsert else '')
                    allbulkinserts.write ( bulktrace.getvalue() )    
                    firstinsert = False
                else:
                    self.logger.warning ( "rejecting %s (too many traces)" % filepath ) 
                    self.mycursor.execute ( Seedtree.__SQL_INSERT_REJECTED_FILE, [ fileindex, 'too many traces'] )
            # when the stringIO becomes too big in RAM, flush it to database
            if allbulkinserts.tell()/1024.0**2 > seedtree_config.BULK_INSERT_MAX_SIZE:
                allbulkinserts = self.flush_to_database ( allbulkinserts )
                firstinsert = True
            # logging
            donefiles += len(results)
            if ( time.gmtime().tm_min in range(0,59,7) ):
                elapsed = time.time() - start
                rate = donefiles / ( elapsed / 60.0 )
                self.logger.info ( "%d/%d files done, average = %.1f files/mn, estimated finish in %.0f mn" % ( 
                    donefiles, len(filelist), rate, ( len(filelist) - donefiles ) / rate ) )
                
        # flush remaining traces to database
        self.flush_to_database ( allbulkinserts )
        pool.close()
        
        # regenerate indexes
        if dropindex:
            self.logger.info("Creating Traces index...")
            self.mycursor.callproc('''create_index_on_traces''')

        # refresh views
        self.logger.info("refreshing traces views and updated channels view...")
        self.mycursor.callproc ( '''refresh_traces_views''' )

        self.logger.info("traces updated (post-computed datas may need to be re-processed)")    
        return None

    def compute_products ( self, force=False ):
        """refresh various post-computed products"""
        if force:
            self.logger.info("forcing products update for all channels...")    
            self.mycursor.callproc("force_channels_update")

        # get updated channels
        updatedchannels = self.get_updated_channels( style='with_quality' )
    
        # delete obsolete products and re-compute products.
        # deletion and re-computation of products is split by channel and by year : this is 
        # a simple optimization allowing not to recompute every products for every years at each run. 
        self.logger.info("deleting and recomputing obsolete products...")    
        # for each channel,
        for c in updatedchannels:
            # for each year where an update happened,
            for y in range ( int(c.updated_from_year), int(c.updated_to_year) + 1 ):
                # delete obsolete products
                self.logger.debug("deleting products for %s.%s.%s.%s %s for year %d" % (c.network,c.station,c.location,c.channel,c.quality, y)) 
                self.mycursor.callproc ( "delete_obsolete_products_by_year", [ c.network,c.station,c.location,c.channel,c.quality, y ] )
                # compute and store gaps
                self.logger.debug("computing gaps for %s.%s.%s.%s %s year %d" % (c.network,c.station,c.location,c.channel,c.quality, y))
                bulk = self.compute_channel_gaps ( c.network,c.station,c.location,c.channel,c.quality, y )
                bulk.flush()
                bulk.seek(0)
                self.mycursor.copy_from ( bulk, 'gaps', columns = self.__SQL_GAPS_COLUMNS )
                bulk.close()
                # compute and store data availability
                self.logger.debug("computing availability for %s.%s.%s.%s year %d" % (c.network,c.station,c.location,c.channel, y))
                bulk = self.compute_data_availability ( c.network,c.station,c.location,c.channel, y )
                if bulk:
                    bulk.flush()        
                    # FIXME debug
                    #with open ('dataavailability.dump', 'w') as fd:
                    #    bulk.seek(0)
                    #    fd.write (bulk.getvalue ())
                    # end debug 
                    bulk.seek(0)
                    self.mycursor.copy_from ( bulk, 'dataavailability', columns = self.__SQL_DATAAVAILABILITY_COLUMNS )
                    bulk.close()

        # reset updated channels
        self.logger.info("reseting channels update flag...")    
        self.mycursor.callproc ( "reset_updated_channels" )

        # commit changes
        self.logger.info("commiting changes")
        self.myconnection.commit()

#    def compute_gaps ( self, channels ):    
#        """compute and store gaps for each channel in channels"""
#        # compute gaps for each channel and quality
#        for c in channels:
#            self.logger.info( "computing gaps for %s.%s.%s.%s quality %s timespan %s → %s" % (c.network,c.station,c.location,c.channel,c.quality,c.starttime,c.endtime))
#            bulkgaps = self.compute_channel_gaps ( c )
#            bulkgaps.flush()
#            bulkgaps.seek(0)
#            self.mycursor.copy_from ( bulkgaps, 'gaps', columns = self.__SQL_GAPS_COLUMNS )
#            bulkgaps.close()
                     
    def compute_channel_gaps ( self, network, station, location, channel, quality, year ):
        """
        compute gaps/overlaps for a single channel and for a full year. 
        Return value is a StringIO object fitted for database bulk insert (COPY FROM).
        """               
        # string for bulk inserts (SQL "copy from") 
        bulkinsert = StringIO()
        firstline = True      

        # get traces
        self.mycursor.execute ( self.__SQL_SELECT_TRACES3, (network, station, location, channel, quality, year, year) )
        traces = self.mycursor.fetchall()
        previoustrace = None
        # for each trace,
        for currenttrace in traces:
            if previoustrace:
                # skip iteration if both traces does not have same sample rate
                if previoustrace.samplerate != currenttrace.samplerate: 
                    self.logger.warning('found variable sample rate (%.1fHz %.1fHz) between two traces (%s %s %s %s)' % 
                        (previoustrace.samplerate, currenttrace.samplerate, network, station, location, channel))
                    continue
                # skip iteration if samplerate is 0
                if previoustrace.samplerate == 0.0:
                    self.logger.warning ('sample rate 0Hz found on trace (%s %s %s %s). Skipping iteration.' % (network, station, location, channel) )
                    continue
                # compare to previous trace
                expected_deltaseconds = 1.0 / previoustrace.samplerate
                true_deltaseconds = ( currenttrace.starttime - previoustrace.endtime ).total_seconds()
                ratio = true_deltaseconds / expected_deltaseconds
                # tolerate non-significant ratio values (arbitrary values)
                if not ( 0.98 <= abs(ratio) <= 1.02 ):
                    # store gap as a string suitable for COPY FROM command
                    # note : '%f' to convert from exponential notation to straigth decimal notation   
                    t = ( network, station, location, channel, 
                        previoustrace.traceindex, currenttrace.traceindex, 
                        previoustrace.endtime, currenttrace.starttime,
                        previoustrace.dirname, previoustrace.basename,
                        currenttrace.dirname, currenttrace.basename,
                        previoustrace.quality, currenttrace.quality,
                        expected_deltaseconds,
                        '%f' % true_deltaseconds,
                        ratio
                        )
                    bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in t) )
                    firstline = False
            previoustrace = currenttrace
        return bulkinsert

    def compute_data_availability ( self, network, station, location, channel, year ):
        """
        Compute data availability for channel, 
        Data availability correponds to any time window where data is available, 
        whatever is its quality, sample rate, recordlength. 
        FIXME This function is almost the same as compute_channel_gaps(), code could be refactored.
        """
        # string for bulk inserts (SQL "copy from") 
        bulkinsert = StringIO()
        firstline = True      
                
        #print "CHANNEL %s ~~~~~~~~~~~~~~~~~~~" + '.'.join([network,station,location,channel,str(year)])

        # get all traces (traces are sorted by {starttime ; endtime } )
        self.mycursor.execute ( self.__SQL_SELECT_TRACES2, (network, station, location, channel, year, year ) )
        traces = self.mycursor.fetchall()
        previoustrace = None
        currentstarttime = None
        currentendtime = None
        
        if not traces: 
            self.logger.debug("no traces found for %s.%s.%s.%s year %d" % (network,station,location,channel, year))
            return None
        # for each trace, 
        for currenttrace in traces:
            # print "\tnew trace %s => %s %s" % (currenttrace.starttime, currenttrace.endtime, currenttrace.quality)
            # first time in the loop : initialize availability markers
            if not previoustrace:
                availabilitystart = currenttrace.starttime
                availabilityend = currenttrace.endtime
            # compare to previous trace
            if previoustrace:                    
                # skip iteration if samplerate is 0
                if previoustrace.samplerate == 0.0:
                    self.logger.warning ('sammplerate 0Hz found on trace (%s %s %s %s). Skipping iteration.' % (network, station, location, channel) )
                    continue
                expected_deltaseconds = 1.0 / previoustrace.samplerate
                true_deltaseconds = ( currenttrace.starttime - previoustrace.endtime ).total_seconds()
                ratio = true_deltaseconds / expected_deltaseconds
                # compare current time window to previous trace
                # if both are continuous (or overlapping), merge time windows together
                if ratio <= 1.02:
                    availabilityend = currenttrace.endtime
                    # print "\tmerge!"
                # else we have a gap : record data availability, record gap, start a data availability time window 
                else:
                    a = ( network, station, location, channel, availabilitystart, availabilityend, 'data' )
                    g = ( network, station, location, channel, availabilityend, currenttrace.starttime, 'gap' )
                    availabilitystart = currenttrace.starttime
                    availabilityend = currenttrace.endtime
                    bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in a) )
                    firstline = False
                    bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in g) )
                    # print "\tgap!"
                    #print "\tnow WRITE availability %s => %s ========== " % (a[4], a[5])
                    #print "\tnow WRITE gap          %s => %s ========== " % (g[4], g[5])
            previoustrace = currenttrace
            # print "\tcur AVAIL %s => %s" % (availabilitystart, availabilityend)

        # there's one remaining data availability to store
        a = ( network, station, location, channel, availabilitystart, availabilityend, 'data' )
        bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in a) )
        return bulkinsert

        # g = ( c.network, c.station, c.location, c.channel, availabilityend, currenttrace.starttime, 'gap' )
        # availabilitystart = currenttrace.starttime
        # availabilityend = currenttrace.endtime
        # firstline = False
        # bulkinsert.write ( ('' if firstline else '\n') + '\t'.join(str(item) for item in g) )
        # print "\nQuitting loop"
        # print "\tnow WRITE availability %s => %s ========== " % (a[4], a[5])
                    
        # dump to database
        #bulkinsert.flush()        
        # FIXME debug
        #with open ('dataavailability.dump', 'w') as fd:
        #    bulkinsert.seek(0)
        #    fd.write (bulkinsert.getvalue ())
        # end debug 
        #bulkinsert.seek(0)
        #self.mycursor.copy_from ( bulkinsert, 'dataavailability', columns = self.__SQL_DATAAVAILABILITY_COLUMNS )
        #bulkinsert.close()
          
    def printstats ( self, minratio, maxratio, net = None, sta = None):
          """print various statistics for all network/stations/loc/chan"""
          networks = self.get_networks()
          if not networks: return
          root = 'Database statistics\nlast insert on %s' % self.get_last_scan_date().strftime("%Y-%m-%d %H:%M")
          tree = { root : OrderedDict() }
          # FIXME plutot que ce nid de boucle, pourquoi ne pas utiliser get_all_channels() ? 
          for network in networks:
            if net and network != net: continue
            tree[root][network] = OrderedDict()
            stations = self.get_stations ( network )
            for station in stations:
                if sta and station != sta: continue
                tree[root][network][station] = OrderedDict()                        
                locations = self.get_location_codes ( network, station )
                for location in locations:
                    tree[root][network][station][location] = OrderedDict()                        
                    channels = self.get_channels ( network, station, location )
                    for channel in channels:
                        # get all gaps, plus informations on qualities and samples 
                        gaps = self.get_gaps ( network, station, location, channel, minratio, maxratio, None, None, None)
                        qualities_and_samples = self.get_qualities_and_samples ( network, station, location, channel )
                        # build a dictionary containing all qualities information
                        qdict = OrderedDict()
                        for q in qualities_and_samples:
                            qdict['quality ' + q.quality + ', ' + 'recordlength ' + str(q.recordlength) + ', '+ str(q.samplerate) + 'Hz, ' + str(q.totaltraces) + ' traces, '\
                            + str(q.totalsamples) + ' samples' + ', ' + q.starttime.strftime("%Y-%m-%d %H:%M") + ' to ' + q.endtime.strftime("%Y-%m-%d %H:%M") ] = {}
                        # complete tree with data availability, number of gaps, and qdict 
                        tree[root][network][station][location][channel] = OrderedDict (
                            { 'Data available from ' + self.get_channel_mintime ( network, station, location, channel, None).isoformat()
                            + ' to ' + self.get_channel_maxtime ( network, station, location, channel, None).isoformat() : {}, 
                            'Gaps and/or overlaps : ' + str (len(gaps))  : {}, 'Qualities and samples' : qdict }          
                            )
          tr = LeftAligned()
          print tr ( tree )  

    def printrejected ( self ):
        self.mycursor.execute ( self.__SQL_GET_REJECTED_FILES )
        for l in self.mycursor.fetchall(): print l.rejected, '\t#', l.comment
        
    def __init__ ( self, schema ):
        """
        Initialize a Seedtree instance. Argument must be an existing database schema. 
        """
        self.myschema = schema
        seedtree_config.set_database_schema ( self.mycursor, self.myschema )
        seedtree_config.check_database_version ( self.mycursor )
        # logging - do not use any rotating-like handler here for thread safety. 
        self.logger = logging.getLogger (__name__)
        self.logger.propagate = False                
        handler = logging.FileHandler( seedtree_config.LOGFILE )
        handler.setFormatter( logging.Formatter ( seedtree_config.LOGFORMAT ) ) 
        self.logger.addHandler ( handler ) 
        self.logger.setLevel ( seedtree_config.LOGLEVEL )
        self.logger.info("Seedtree v%s starting on schema %s ----------" %  (self.SCRIPT_VERSION,schema))

    def __iter__(self):
      """this class is able to iterate over database objects (e.g. miniseed traces)"""
      return self

    def close (self):
        """
        Close Seedtree instance (= close database connection).
        Will trigger an implicit rollback from the backend
        """        
        self.mycursor.close()
        self.myconnection.close()
        self.logger.info("database connection closed (schema %s)" % self.myschema)
                
    def __str__ (self):
      return ( "Seedtree instance on schema " + self.myschema )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if __name__ == "__main__":

    import seedtree5
        
    # build command line parser
    parser = argparse.ArgumentParser(
        description='Computes and retrieves informations from miniseed traces database.',
        epilog='My configuration file is ' + seedtree_config.__file__ + '\n. Do not use my stdout for automatic parsing (use my Python API instead).')
    group1 = parser.add_mutually_exclusive_group ( required = True )
    group1.add_argument("--sync", help='Update miniseed traces (should be run after Filetree scan update)', action='store_true')
    group1.add_argument("--computeproducts", help='Recompute various products derived from a scan (gaps, data availability...). Should be run after --sync command.', action='store_true')
    group1.add_argument("--gettraces", help='Print miniseed traces according to optional search parameters.', action='store_true')
    group1.add_argument("--getgaps", help='Print gaps/overlaps, according to optional search parameters.', action='store_true')
    group1.add_argument("--getdataavailability", help='Print data availability, according to optional search parameters', action='store_true')
    group1.add_argument("--printstats", help='Print data statistics (may be filtered with --network and --station)', action='store_true')
    group1.add_argument("--printrejected", help='Print rejected miniseed files', action='store_true')
    group1.add_argument("--getlastscandate", help='Print latest date an insert was made into the schema', action='store_true')

    group2 = parser.add_argument_group('Search parameters')
    group2.add_argument("--network", help='Print traces belonging to this network code only')
    group2.add_argument("--station", help='Print traces for this station only')
    group2.add_argument("--location", help='Print traces for this location code only')
    group2.add_argument("--channel", help='Print traces for this location code only')
    group2.add_argument("--quality", help='Print traces for this quality only (expected : D,R,M, or Q)')
    group2.add_argument("--starttime", help='Print traces containing data at this date and after. Starttime should be lower than Endtime argument. Recommended date format is ISO8601 : YYYY-MM-DDThh:mm:ss[.mmmm]', type=myTools.isvaliddate)
    group2.add_argument("--endtime", help='Print traces containing data at this date and before', type=myTools.isvaliddate)
    group2.add_argument("--samplerate", help='Print traces with this sample rate only',type=float)
    group2.add_argument("--recordlength", help='Print traces with this record length only (eg. 512, 4096)',type=int)
    group2.add_argument("--minratio", help='Print gaps/overlaps with ratio >= minratio (>1.0 is a gap, <1.0 is an overlap)',type=float)
    group2.add_argument("--maxratio", help='Print gaps/overlaps with ratio <= maxratio (>1.0 is a gap, <1.0 is an overlap)',type=float)

    group3 = parser.add_argument_group('Miscellaneous parameters')
    group3.add_argument("--force", help='Force update of all products when using --computeproducts', action='store_true')
    group3.add_argument("--ignoretimeout", help='Ignore timeout limit (see configuration file) when running --sync', action='store_true', default=False)

    parser.add_argument ("schema", metavar='SCHEMA',
            help='Databse schema name to use (a schema is a kind of namespace. See PostgreSQL documentation. Use "seedtree-admin" to manage schemas.)' )
    parser.add_argument ("--version", action='version', version = 'v%.3f using database v%.3f' % ( Seedtree.SCRIPT_VERSION, seedtree_config.DB_VERSION ))    
    args = parser.parse_args()

    # create a Seedtree instance
    mytree = seedtree5.Seedtree ( args.schema )
    
    # sync database
    if args.sync: mytree.sync(args.ignoretimeout)
        
    # print traces
    if args.gettraces:
        mytree.open_traces(args.network, args.station, args.location, args.channel, args.starttime, args.endtime, args.samplerate, args.quality, args.recordlength)
        print '\t' . join (('Index','Network.Station.Loc.Channel', 'Filename', 'Quality', 'Samplerate', 'Recordlength', 'Starttime → Endtime'))
        for t in mytree: 
            print '\t'.join ( (str(t.traceindex), '.'.join((t.network,t.station,t.location,t.channel)),
            os.path.join(t.dirname, t.basename), t.quality, str(t.samplerate), str(t.recordlength), str(t.starttime) + ' → ' + str(t.endtime)))  
        
    # compute products
    if args.computeproducts: mytree.compute_products(args.force)
        
    # print gaps
    if args.getgaps : 
        gaps = mytree.get_gaps(args.network, args.station, args.location, args.channel, args.minratio, args.maxratio, args.starttime, args.endtime, args.quality)
        simplegap =  namedtuple('SimpleGap', 'net sta loc cha qlty Mhz previous_endtime current_starttime julian previous_basename current_basename time_delta ratio')
        # workaround printing of timedelta object, because of negative values normalization (see Python documentation)
        simplegaps = [ simplegap (g.network, g.station, g.location, g.channel, 
                g.previous_quality + "-" + g.current_quality, 1.0 / g.expecteddelta.total_seconds(),
                g.previous_endtime, g.current_starttime, 
                str(g.previous_endtime.timetuple().tm_yday) + '-' + str(g.current_starttime.timetuple().tm_yday),
                g.previous_basename, g.current_basename,  
                ('-' if g.ratiodelta < 0 else '+') + str(abs(g.truedelta))[:15] , g.ratiodelta) for g in gaps ]
        if simplegaps: print tabulate (simplegaps, headers="keys", tablefmt="simple")

    # print data availability
    if args.getdataavailability:
        data = mytree.get_dataavailability(args.network, args.station, args.location, args.channel, args.starttime, args.endtime)
        print '\t' . join (('Network.Station.Loc.Channel', 'Starttime → Endtime'))
        for t in data:
            print '\t'.join ( ('.'.join((t.network,t.station,t.location,t.channel)), str(t.starttime) + ' → ' + str(t.endtime) ) )
                
    # print statistics
    if args.printstats : mytree.printstats ( args.minratio, args.maxratio, args.network, args.station )    

    # print rejected files
    if args.printrejected : mytree.printrejected()

    # print last scan date
    if args.getlastscandate: print mytree.get_last_scan_date().strftime("%Y-%m-%d %H:%M:%S")
   
    mytree.close()
     
   
