#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Python standard library
import argparse
import logging
# import magic
import os
import socket
import stat
import sys
import traceback

from collections import namedtuple
from datetime import datetime
from cStringIO import StringIO

import psycopg2 as dbapi2
import psycopg2.extras

# https://pypi.python.org/pypi/tabulate
from tabulate import tabulate

import myTools
import seedtree_config

class Filetree:
    """
    Utility for scanning files attributes (mainly POSIX attributes) and maintaining a file index. 
    Uses PostgreSQL to store data. 
    """

    # script version number is a float : YYYY.julian_day
    SCRIPT_VERSION = 2016.314
        
    # database parameters
    myconnection = seedtree_config.DB_CONNECTION
    mycursor = seedtree_config.DB_CURSOR
    myschema = None
    
    # NamedTuple to gather scan results 
    ScanResult = namedtuple ( "ScanResult", "totalentries skippedentries newfiles updatedfiles deletedfiles fileindexes" )
    myScanResult = ScanResult ( None, None, 0, 0, 0, None )
    
    # SQL queries
    __SQL_SEARCH_FILE_PATH_OCCURENCE = '''file_search_occurence'''
    
    __SQL_FILES_NEXTVAL = '''SELECT nextval(pg_get_serial_sequence('files', 'fileindex'))'''
    __SQL_FILES_COLUMNS = ('fileindex','dirname','basename','filesize','datemodified','checksum','mimetype')
    
    __SQL_SEEK_FILE_INDEX = '''SELECT fileindex,dirname, basename FROM files WHERE fileindex'''
    __SQL_DELETE_FILE = '''DELETE FROM files WHERE fileindex IN '''
    __SQL_DELETE_FILE_NOT_IN = '''DELETE FROM files WHERE fileindex NOT IN '''

    __SQL_SELECT_LAST_SCAN = '''SELECT max(endtime) AS endtime FROM logs'''
    __SQL_SELECT_SCAN_DATES = '''SELECT action, endtime, totalentries, skippedentries, newfiles, updatedfiles, deletedfiles FROM logs ORDER BY endtime DESC'''
    __SQL_INSERT_LOG = '''INSERT INTO logs VALUES ( %s, now(), clock_timestamp(), %s, %s, %s, %s, %s, %s )'''
    
    # logging
    mylogger = None
    
    # API functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def get_journal_without_duplicates ( self ):
        """
        FIXME
        """
        self.mycursor.execute ( "LOCK TABLE journal" )
        query = '''SELECT fileindex,dirname,basename,event FROM journal ORDER BY fileindex,journalstamp ASC'''
        self.mycursor.execute ( query )
        rows = self.mycursor.fetchall()
        self.mylogger.info( "got %s events from journal", len ( rows ) )
        # remove duplicate file indexes (meaning : keep only the latest journal event for each fileindex)
        journaldict = {}
        for j in rows: journaldict [ j.fileindex ] = j
        return journaldict.values()        

    def empty_journal ( self ):
        """
        FIXME
        """
        self.mylogger.info( "deleting journal" )
        self.mycursor.execute ( 'DELETE FROM journal' )
        return None
            
    def get_last_scan_date ( self ):
        """Returns the most recent date this schema had any type of scan.
        (None if never scanned before)"""
        self.mycursor.execute ( self.__SQL_SELECT_LAST_SCAN )
        row = self.mycursor.fetchone()
        return row.endtime if row else None

    def get_all_scan_dates ( self ):
      """Returns all scans details (None if never scanned before)"""
      self.mycursor.execute ( self.__SQL_SELECT_SCAN_DATES )
      rows = self.mycursor.fetchall()      
      return rows

    def scan ( self, insert, delete, sync, forceupdate ):
        """
        Main wrapper for scan/update/delete operations. Paths are expected on stdin.
        Returns 0 if no error, else raises exception.
        """ 
        stamp = datetime.now()
        try:
            # run insert or sync command
            if insert or sync: 
                result = self.insert(stamp, forceupdate)
                self.update_logs ( 'insert', result ) 
                if sync:
                    result = self.delete_indexes ( result.fileindexes, stamp, invert = True )
                    self.update_logs ( 'delete(sync)', result )     
            # run delete command 
            elif delete: 
                # seek for indexes, then delete indexes
                result = self.search_indexes()
                result = self.delete_indexes ( result.fileindexes, stamp )
                self.update_logs ( 'delete', result )     
            # commit changes to DB
            self.mylogger.info("commiting changes")
            self.myconnection.commit()
        # Unknown errors
        except:
            self.mylogger.critical("unknown error (%s), rolling back DB" % traceback.format_exc().replace('\n','|'))
            self.myconnection.rollback()
            raise
        return 0

    # END OF API ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                             
    def insert ( self, journalstamp, forceupdate = False ):
        """insert files into database"""       
        # counters
        nbtotalentries = 0
        nbskippedentries = 0
        nbnewfiles = 0
        nbupdatedfiles = 0
        # string for bulk inserts (SQL "copy from") 
        bulkinserts = StringIO()
        # list of indexes to keep
        keepindexes = []
        # read stdin for file paths
        for fullpath in sys.stdin:
            try:
                nbtotalentries += 1
                fullpath = fullpath.rstrip('\n')
                basename = os.path.basename ( fullpath )
                dirname = os.path.dirname ( fullpath )
                # lstat will not follow symlinks
                # broken links are reported as OSerror
                try: statinfo = os.lstat ( fullpath )
                except OSError as e:
                    self.mylogger.error ( str(e) )
                    continue
                # is it a regular file ? 
                if not stat.S_ISREG ( statinfo[stat.ST_MODE] ): 
                    self.mylogger.warning("skipping %s (not a regular file)" % fullpath)
                    nbskippedentries += 1
                    continue
                # normalize path
                if seedtree_config.NORMALIZE_PATH: dirname = os.path.abspath(dirname)
                # convert st_mtime to a long int to avoid problems with float type in the DB
                currentLastModify = long ( round ( statinfo.st_mtime * 1E6 ) )
                currentSize = statinfo.st_size
                # compute checksum 
                currentChecksum = str ( myTools.adler32(fullpath) ) if seedtree_config.COMPARE_BY_CHECKSUMS else None
                
                # at first, consider this file is already in the database 
                # and its metadata has not changed
                createFile = False
                updateFile = False
                
                # is this file already known from a previous scan ?
                self.mycursor.callproc ( self.__SQL_SEARCH_FILE_PATH_OCCURENCE, [ dirname, basename ] )
                row = self.mycursor.fetchone() 
                
                # it's a new file
                if not row.fileindex: createFile = True
                # not a new file : was it modified since last scan ?
                else: updateFile = forceupdate or \
                        ( currentChecksum != row.checksum if seedtree_config.COMPARE_BY_CHECKSUMS else \
                        ( currentLastModify != row.datemodified or currentSize != row.filesize )) 
                if createFile or updateFile:
                    self.mylogger.debug("%s %s %s" % ('+' if createFile else '~', dirname, basename))                        
                    # FIXME mimetype sometimes makes problem because of unexpected characters. 
                    # FIXME => giving up, try to fix this later
                    # mimetype = magic.from_file ( fullpath )
                    # mimetype = mimetype.decode('unicode_escape').encode('ascii','replace')
                    # mimetype = dbapi2.extensions.QuotedString(mimetype) if mimetype else '\\N'
                    mimetype = '\\N'           
                if updateFile:
                    self.mycursor.execute ( \
                        "UPDATE files SET filesize=%s, datemodified=%s, checksum=%s, mimetype=%s, lastupdated=clock_timestamp() WHERE fileindex=%s", \
                        [currentSize, currentLastModify, currentChecksum, mimetype, row.fileindex] )
                    self.mycursor.execute("INSERT INTO journal (journalstamp, fileindex, dirname, basename, event) VALUES (%s,%s,%s,%s,%s)", [journalstamp,row.fileindex,dirname,basename, 'update'])
                    # keep track of this index
                    keepindexes.append ( row.fileindex ) 
                    nbupdatedfiles +=1
                if createFile:
                    # get a new file index, keep track of it
                    self.mycursor.execute ( self.__SQL_FILES_NEXTVAL )
                    row = self.mycursor.fetchone()
                    # builds a string suitable for COPY FROM command       
                    bulkinserts.write( ('' if not nbnewfiles else '\n')
                        + '\t'.join (str(x) for x in (row.nextval, dirname, basename, currentSize, currentLastModify, 
                        currentChecksum if currentChecksum else '\\N', mimetype)))
                    self.mycursor.execute("INSERT INTO journal (journalstamp, fileindex, dirname, basename, event) VALUES (%s,%s,%s,%s,%s)", [journalstamp, row.nextval,dirname,basename, 'create'])
                    # keep track of this new index
                    keepindexes.append ( row.nextval )
                    nbnewfiles += 1
                # file already exists and was not updated : keep track of its index
                if not updateFile and not createFile :
                    keepindexes.append ( row.fileindex ) 
                # do some logging then proceed to next stdin entry
                if ( not nbtotalentries % 100 ): 
                    self.mylogger.info("status : %d entries, %d inserts, %d updates" % (nbtotalentries, nbnewfiles, nbupdatedfiles) )
            # strange filenames (not fatal)
            except psycopg2.DataError:
                self.mylogger.error("psycopg DataError :" % fullpath)
                continue

        # bulk-inserts new files
        if nbnewfiles:
            logging.info("inserting %d new files..." % nbnewfiles )
            # logging.info ( "bulk insert size = %.2fMb" % (bulkinserts.tell()/1024.0**2) )
            bulkinserts.seek(0)
            self.mycursor.copy_from ( bulkinserts, 'files', columns = self.__SQL_FILES_COLUMNS )

        bulkinserts.close()
                    
        # return scan results
        return ( self.myScanResult._replace ( totalentries=nbtotalentries, skippedentries=nbskippedentries,
            newfiles=nbnewfiles, updatedfiles=nbupdatedfiles, deletedfiles=0, fileindexes=keepindexes) )

    def search_indexes ( self ) :
        """Reads stdin for file paths, return corresponding file index in database"""
        # list of all file indexes processed so far
        obsoleteindexes = list()
        nbtotalentries = 0
        # read stdin for file paths
        for fullpath in sys.stdin:
            nbtotalentries += 1
            fullpath = fullpath.rstrip('\n')
            basename = os.path.basename ( fullpath )
            dirname = os.path.dirname ( fullpath )
            if seedtree_config.NORMALIZE_PATH: dirname = os.path.abspath(dirname)
            # is this file already known from a previous scan ?
            self.mycursor.callproc ( self.__SQL_SEARCH_FILE_PATH_OCCURENCE, [ dirname, basename ] )
            row = self.mycursor.fetchone()
            # unknown file
            if not row.fileindex: self.mylogger.error("! %s %s " % (dirname,basename))
            # known file : append to index list
            else: 
                self.mylogger.debug("- %s %s" % (dirname, basename))                        
                obsoleteindexes.append ( row.fileindex )
        return ( self.myScanResult._replace ( totalentries=nbtotalentries, fileindexes=obsoleteindexes) )
            
    def delete_indexes ( self, indexes, journalstamp, invert = False ):
        """
        Delete from the DB all files indexes listed in 'indexes'.
        If invert is True, deletes files that are *not* in 'indexes'.
        FIXME not efficient and ugly code : could make better/faster sql. See:
        FIXME http://dba.stackexchange.com/questions/34864/most-efficient-way-of-bulk-deleting-rows-from-postgres
        FIXME http://stackoverflow.com/questions/8290900/best-way-to-delete-millions-of-rows-by-id
        """
        self.mylogger.info ( "deleting obsolete indexes (%d indexes to %s)" % ( len(indexes), 'retain' if invert else 'delete') )
        # delete files from db, return number of deleted rows
        # if indexes is empty, return empty result 
        if invert and indexes:
            indexesString = '(' + ',' . join(str(x) for x in indexes) + ')'
            query = '''SELECT fileindex from files WHERE fileindex NOT IN ''' + indexesString
            self.mycursor.execute ( query )
            indexes = [ x.fileindex for x in self.mycursor.fetchall() ]
        if not indexes: return ( self.myScanResult ) 
        indexesString = '(' + ',' . join(str(x) for x in indexes) + ')'
        query = self.__SQL_DELETE_FILE + indexesString
        self.mycursor.execute ( query  )
        # update journal
        for i in indexes:
            self.mycursor.execute("INSERT INTO journal (journalstamp, fileindex, dirname, basename, event) VALUES (%s,%s,%s,%s,%s)", [journalstamp, i, None, None, 'delete'])
        return ( self.myScanResult._replace ( totalentries=len(indexes) if not invert else -len(indexes), deletedfiles = self.mycursor.rowcount ) )

    def update_logs ( self, action, result ):
        """update logs according to scan results"""
        self.mylogger.info("%s finished | entries total=%s skipped=%s | files new=%s updated=%s deleted=%s" %
            ( action, result.totalentries, result.skippedentries, result.newfiles, result.updatedfiles, result.deletedfiles) )  
        t = [ action, socket.gethostname(), result.totalentries, result.skippedentries, 
            result.newfiles, result.updatedfiles, result.deletedfiles ] 
        self.mycursor.execute ( self.__SQL_INSERT_LOG, t )
                                       
    def __init__ ( self, schema ):
        self.myschema = schema
        # logging - do not use any rotating-like handler here for thread safety. 
        self.mylogger = logging.getLogger (__name__)
        self.mylogger.propagate = False        
        handler = logging.FileHandler( seedtree_config.LOGFILE )
        handler.setFormatter( logging.Formatter ( seedtree_config.LOGFORMAT ) ) 
        self.mylogger.addHandler ( handler ) 
        self.mylogger.setLevel ( seedtree_config.LOGLEVEL )
        self.mylogger.info("Filetree v%s starting on schema %s ----------" %  (self.SCRIPT_VERSION,schema))
        try:
            seedtree_config.set_database_schema ( self.mycursor, self.myschema )
            seedtree_config.check_database_version ( self.mycursor )
        except Exception as e:
            self.mylogger.critical ( e )
            raise

    def close (self):
        """
        Close Filetree instance (= close database connection).
        Will trigger an implicit rollback from the backend
        """        
        self.mycursor.close()
        self.myconnection.close()
        self.mylogger.info("database connection closed (schema %s)" % self.myschema)
          
    def __str__ (self):
        return ( 'Filetree on schema %s' % self.myschema ) 

    # MAIN ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    if __name__ == "__main__":
        import filetree
        parser = argparse.ArgumentParser ( description = 'Scan files, builds and updates a file index database. Files paths are expected on STDIN (eg. using find command). Configuration is read from seedtree_config.py module.' )
        group = parser.add_mutually_exclusive_group ( required = True )
        group.add_argument("--insert", help='insert/update files in the index', action='store_true')
        group.add_argument("--sync", help='same as --insert, but also deletes from the index any file that is not on stdin', action='store_true')
        group.add_argument("--delete", help='delete files from the index', action='store_true')
        group.add_argument("--printstats", help='print scans statistics', action='store_true')
        parser.add_argument("--forceupdate", help='when used with --insert or --sync, force update of files', action='store_true')
        parser.add_argument ("--version", action='version', version = 'v%.3f using database v%.3f' % ( SCRIPT_VERSION, seedtree_config.DB_VERSION ))
        parser.add_argument ("schema", metavar='SCHEMA',
            help='Database schema name to use as a logical store. Use "seedtree-admin" to manage schemas.' )
        args = parser.parse_args()
        # build Filetree instance and run actions
        mytree = filetree.Filetree ( schema = args.schema )
        if args.insert or args.delete or args.sync: mytree.scan ( insert=args.insert, delete=args.delete, sync=args.sync, forceupdate = args.forceupdate ) 
        if args.printstats:
            s = mytree.get_all_scan_dates()
            print tabulate ( s, headers="keys", tablefmt="simple" ) if s else "No logs."
            
        mytree.close()
