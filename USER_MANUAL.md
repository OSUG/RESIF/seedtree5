<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Architecture](#architecture)
- [Terminology](#terminology)
  - [Traces](#traces)
  - [Gaps, Overlaps](#gaps-overlaps)
  - [Data availability](#data-availability)
- [Using database schemas](#using-database-schemas)
    - [Note for RESIF datacentre operators](#note-for-resif-datacentre-operators)
- [Installation and configuration](#installation-and-configuration)
  - [Source code deployment](#source-code-deployment)
  - [Configuration file](#configuration-file)
    - [Security consideration](#security-consideration)
  - [Environment variables](#environment-variables)
  - [Installing on a multi-users system, using multiple configuration files](#installing-on-a-multi-users-system-using-multiple-configuration-files)
- [Using command-line mode](#using-command-line-mode)
    - [Note for developppers](#note-for-developppers)
  - [Handling database schemas](#handling-database-schemas)
  - [Scanning files](#scanning-files)
    - [More examples](#more-examples)
  - [Computing gaps and data availability, and others products](#computing-gaps-and-data-availability-and-others-products)
  - [Checking for rejected files](#checking-for-rejected-files)
  - [Displaying schema statistics](#displaying-schema-statistics)
  - [Getting miniseed traces](#getting-miniseed-traces)
  - [Geting gaps and overlaps](#geting-gaps-and-overlaps)
  - [Getting data availability](#getting-data-availability)
  - [Logging and monitoring code activity](#logging-and-monitoring-code-activity)
- [Using Python API](#using-python-api)
  - [Online documentation](#online-documentation)
  - [Initializing a Seedtree object](#initializing-a-seedtree-object)
    - [Avoiding deadlocks](#avoiding-deadlocks)
  - [Data types](#data-types)
  - [Scanning a set of files](#scanning-a-set-of-files)
  - [Getting the most recent scan date](#getting-the-most-recent-scan-date)
  - [Getting informations about seismic networks](#getting-informations-about-seismic-networks)
    - [Network codes](#network-codes)
    - [Stations codes](#stations-codes)
    - [Stations codes (detailed)](#stations-codes-detailed)
    - [Location codes](#location-codes)
    - [Channels](#channels)
    - [Channels (all)](#channels-all)
    - [Qualities, record length and samples overview](#qualities-record-length-and-samples-overview)
  - [Getting miniseed traces](#getting-miniseed-traces-1)
    - [Date formats](#date-formats)
    - [Trace : tuple structure](#trace--tuple-structure)
  - [Getting first or last trace datetime](#getting-first-or-last-trace-datetime)
  - [Getting gaps (and overlaps)](#getting-gaps-and-overlaps)
    - [Example usages](#example-usages)
  - [Getting data availability](#getting-data-availability-1)
  - [Closing a Seedtree object](#closing-a-seedtree-object)
- [Using the PL/pgSQL API](#using-the-plpgsql-api)
- [Bugs, limitations](#bugs-limitations)
    - [Database locks](#database-locks)
- [Misc tools](#misc-tools)
  - [readmseed.py](#readmseedpy)
  - [msimetadata2json.sh](#msimetadata2jsonsh)
  - [RESIF datacentre toolbox](#resif-datacentre-toolbox)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Introduction

Seedtree is a tool for indexing the characteristics of miniseed files. Seedtree runs through a list of files and collects in a database the metadata of the miniseed files it found. As a result of this *scan*, the Seedtree API can be used by third-party codes to query the database.

Seedtree scan results are available via :
*  its Python API (preferred mean for accessing the information)
*  a command line using the `seedtree5.py` command
*  a PostgreSQL prompt, using the PL/Psql functions provided by the API.

## Requirements
*  `Python 2.7` with the following extra modules : `Psycopg2`, `tabulate`, `asciitree`
*  a `Postgresql` (version 9.5 or above) database
*  an IRIS miniseed inspector (msi) executable (https://github.com/iris-edu/msi)

## Architecture

*Seedtree* basically consists of **two independent scripts**, that can be used separately or in combination :

*  `src/filetree.py` : general script to walk through files, detect changes, and update the database with POSIX characteristics (path, size, modification date, ...) of these files. Filetree can be used as standalone without Seedtree.
*  `src/seedtree.py` : specific script for extracting informations from miniseed-formated files. Seedtree runs on top of Filetree.

The following files come in addition :

*  `sql/schema_init.sql` : a set of SQL and PL/PSQL codes
*  `src/seedtree_config.py` : a configuration file
*  `src/seedtree-admin` : a tool for database schemas management

## Terminology

record|a miniseed record
-----------|---------------
trace|a serie of time-contiguous *records* with identical SEED metadata (read below)
|gap, overlap|time interval between two *traces* whose duration does not correspond to the expected sampling frequency (read below)
data availability|aggregated *traces* representing data continuity, regardless of files edges and quality flags (read below)

### Traces

A *trace* is a serie of *records* ...

*  matching the same names (or *codes*) for *network* - *station* - *location* - *channel*,
*  contiguous in time (taking into account the sampling frequency),
*  with some continuous metadata, for example *quality* flag
*  with a variation of *samplerate* within a tolerance threshold

Traces calculation is performed by *IRIS Miniseed Inspector* (see *manpage* for `msi -T -Q -tf 1 <filename>`).

Calculation of traces is carried out file by file. In other words, there can be no trace overlapping multiple files. A trace is bound to a file to which it belongs. On the other hand, the concept of *data availability* goes beyond the boundary between files.

### Gaps, Overlaps

Seedtree considers a gap conceptually the same as an overlap: the documentation refers to the word "gap" to designate both. 

Seedtree calls __ratio__ the ratio between the time interval observed between two *traces* and the time interval that should theoretically separate them (according to the sampling frequency). Consequently, a ratio equal to 1.0 between two *traces* means these traces are "in line" with the sampling frequency (→ no gap, no overlap). 

A ratio greater than 1.0 is a *gap* (for example: ratio=2.0 corresponds to a gap lasting 2 times the sampling frequency). A ratio less than -1.0 is an *overlap*. A ratio between -1.0 and 1.0 corresponds to *records* whose separation is less than the sampling frequency. 

*Seedtree* uses a filter to skip non-significant ratio values (see function `seedtree5.py@compute_channel_gaps`). *Seedtree* computes gaps only between traces with identical quality flags.

### Data availability

Data availability consists in the aggregation of traces. Traces with same SEED identifier(network, station, location, channel) are aggregated together as long as they are not separated by a gap. Data availability does not depend on file border limits, quality flags, or overlaps.

In other words, data availability correponds to any time window where data is available, whatever is its quality, sample rate, recordlength. 

## Using database schemas

Seedtree stores its information in a Postgres database organized using *schemas* (http://www.postgresql.org/docs/9.3/static/ddl-schemas.html). Schemas make it possible to compartment the database according to logically coherent sets.

Schemas have an arbitrary name: the user decides upon which schema(s) *Seedtree* will store its information. Typically, a schema will be created for each observation network, or a specific schema might fit a full year of data for all networks. This decision (how many schemas + how to name and organize them) is essentially guided by performance and convenience factors.

#### Note for RESIF datacentre operators

Naming convention used for schemas is : 

* one schema per seismic observation network 
* schema takes the same name as the full network code (FDSN style), with lowercase characteres, and prefixed by *underscore* symbol (exemple : `_ra`, `_fr`, `_yp2012`, `_1a2009`).
* you may use [resif_pyinventory](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif_pyinventory) to get schema naming related to RESIF inventory.

## Installation and configuration

### Source code deployment

Simply clone the git repository to any place on your system.
In this documentation, we consider it is copied into your home directory (eg. `~/seedtree5`).
 
### Configuration file

Configuration is stored in a Python file : `seedtree_config.py`.  A `seedtree_config.py.dist` file is provided as a template for your own configuration : copy it to `seedtree_config.py`, edit according to your needs. 

Configuration file permissions must be set to executable. Executing the script provides information about its own content ; it is an easy way to check if the file is in your PATH : 

```
$ seedtree_config.py --help
```

#### Security consideration

Configuration file contains your database password : be sure to protect it.

```
# set read + write + execute permissions only for owner 

chmod 700 seedtree_config.py
```


### Environment variables

*  Path to `seedtree_config.py` must be set in your environment : it must be accessible with PATH **and** PYTHONPATH environment variables.  `$HOME/bin` is a relevant place to store your configuration file, this directory being included in default $PATH on most systems. 
*  Path to `src/` directory must be set in your `PATH` and `PYTHONPATH`.

variable name|usage 
---------|-----------
MSI_PATH|full path to *IRIS msi*
PATH|path to `src/` directory + path to configuration file `seedtree_config.py`
PYTHONPATH|path to `src/` directory + path to configuration file `seedtree_config.py`

Typical `~/.bash_profile` file, with `seedtree_config.py` in `~/bin`, and source distribution cloned in `~/seedtree5` :

```
export PATH=$PATH:$HOME/bin:~/seedtree5/src/
export PYTHONPATH=$PYTHONPATH:$HOME/bin/:~/seedtree5/src/
export MSI_PATH="/usr/local/bin/msi"
```

### Installing on a multi-users system, using multiple configuration files

For system-wide installation, you may want to deploy the code to a public-readable directory, like `/usr/local/seedtree5`, and either set a common configuration file in `/usr/local/bin`, or a per-user configuration file in their own `$HOME/bin`.

In other words, you may use various configuration files for a same *Seedtree* deployment, then switch from one to another altering environment variables. To check which configuration file *Seedtree* is currently using, run the online help : 

```
$ seedtree5.py --help
(...)
Configuration file is /home/joe/bin/seedtree_config.pyc 
(...)
```

## Using command-line mode

Online documentation is available for each script : this is where to get the most up-to-date command arguments. Examples below are given as an indication only.

```	
$ ./filetree.py --help
$ ./seedtree5.py --help
```

#### Note for developppers
 
Seedtree command line tools outputs informations on *stdout* designed for interactive usage. Except when expressly specified, it is not advised to parse these outputs for automatic processing (output formats may change). Use the Python API instead.
 
### Handling database schemas

Schemas are managed with `seedtree-admin` command-line tool :

```
$ seedtree-admin --help

# list existing schemas
$ seedtree-admin --list
```

You must create at least one schema before being to insert new datas. For example:

```
$ seedtree-admin --create mydata "this is a test schema"
```

### Scanning files

Two scripts have distinct and complementary roles :


*  `filetree.py` performs scanning of POSIX characteristics of files provided on `stdin`. **It detect the files that have been added, modified, or deleted since previous scan**. It logs events in a __journal__ available for third-party applications (like *seedtree5.py*).
*  `seedtree5.py` browses the *journal* produced by *filetree*, and subsequently maintains a database with miniseed file characteristics + computed products (such as *gaps* and *data availability*)

The workflow for scanning files with filetree and seedtree is :

*  first, run Filetree
    * provides some files paths to scan on *stdin*. These paths are most often generated with the `find` command. You may use 3 options with filetree : `--insert`, `--delete`, or `--sync`. See online help and examples below.
*  then, run Seedtree
    * when *filetree* scan is done, ask *seedtree* to synchronize its own database with *filetree* scan results (`--sync`).
    * when syncing is done, run *seedtree* again to compute extra products.

Example for a typical scan scenario :

```
# create a schema for storing the results of scan
$ seedtree-admin --create myschema "a temporary test schema"
```

Then, scan files with Filetree :

```
# Command line options are:
#
#  --insert       insert/update files in the index
#  --sync         same as --insert, but also deletes from the index any file
#                 that is not on stdin
#  --delete       delete files from the index
#  --forceupdate  when used with --insert or --sync, force update of files

# Scans a whole directory for files. Adds in the database all new or 
# modified files listed by  the find command and deletes from the 
# database all files that are not listed on stdin. 

$ find temp/my_files/ -type f  | filetree.py --sync myschema

#	
# or : delete a specific file
#

echo temp/my_files/file1.mseed | filetree.py --delete myschema
	
#
# or : add a specific file
#

echo temp/my_files/file2.mseed | filetree.py --insert myschema

#
# note : you may provide many paths using the newline character (\n)
#

echo -e temp/file2.mseed\\nData/another_file.txt | filetree.py --insert test
```

Once the initial Filetree scan has finished, you must launch Seedtree so as to compute/synchronize the calculation of miniseed traces. To do this, use the ``--sync`` option :

```
#
#  --sync                Update miniseed traces (should be run after Filetree
#                        scan update)
# --ignoretimeout        Ignore timeout limits (see configuration file) when
#                        running --sync
#
	
$ seedtree5.py --sync myschema
```

In the example above, *myschema* is the name of the *schema* used to store file/miniseed informations.

#### More examples  

```
# scan BUD for a specific network (all 20xx years)
find /mnt/rawdata/bud/20??/RD/ -type f |  ...


# scan BUD and validated data directories in a single pass, 
find /mnt/data/bynet/RD/ /mnt/rawdata/bud/20??/RD/ -type f | ...
	
# scan files contained in BUD for current year and modified
# during the latest 60 minutes
find /mnt/rawdata/bud/`date +"%Y"`/RD/ -type f -mmin -60  | ...
```

### Computing gaps and data availability, and others products

When a seedtree scan ends, extra *products* should be computed (or at least refreshed) such as gaps, data availability, and any others products computed by seedtree. Seedtree tries to be clever about which products to update, trying not to recompute products that do not need to be recomputed (the --force option may be used to force the recomputation of all products for all timeperiods. This will reset the product database , might be useful in case of database corruption). 

The `--computeproducts` command must be run __after__ a `--sync` command. It is not mandatory to run it after each scan (file indexes, miniseed trace indexes, seismic channels identifiers,  will anyway be up to date after a single `--sync` command).

```
#
#  --computeproducts     Recompute various products derived from a scan (eg:
#                        gaps, ...)
#
#  --force               Force update of all channels products when using
#                        --computeproducts 

$ seedtree5.py --computeproducts data1
```

### Checking for rejected files


*  Filetree does not "reject" any file.  
*  Seedtree may reject miniseed files with too many traces (too many traces can, in some cases, have an impact on the database performances). The trace limit may be changed in the configuration file. Adjustments should be made according to the tradeoff wanted between performance and completeness. 
*  Seedtree also rejects miniseed files based on compute duration. The limit can be changed in the configuration file, and may be ignored at runtime using the `--ignoretimeout` option.
*  Seedtree computes files by *chunks* (chunk size is set in the configuration file). All files in a chunk will be rejected even if only one of them takes too much time to compute. Recomputation may be tried again by setting smaller chunks in the configuration file and/or raising timeout limits and/or re-running computations when the system is less busy. 

```
# displaying miniseed files rejected during previous scans:
$ seedtree5.py --printrejected myschema`</code>`
```

The output shows the path of the rejected file (first column), and the reason for rejection (second column). This output may be reused to attempt re-insertion of rejected file(s) in the database :

```
# force update of previously rejected files 
$ seedtree5.py --printrejected myschema | grep timeout | awk '{print $1}' | filetree.py --insert --force myschema
	
# then recompute miniseed characteristics...
$ seedtree5.py --sync myschema
	
# ...or recompute + ignore timeout limit
$ seedtree5.py --sync --ignoretimeout myschema
```

Manually reduces the number of rejected files:

```
# count number of rejected files
$ seedtree5.py --printrejected myschema |  wc -l
245
	
# force update of the 10 first files
$ seedtree5.py --printrejected myschema | grep timeout | awk '{print $1}' | head -10 | filetree.py --insert --force myschema
$ seedtree5.py --sync myschema
$ seedtree5.py --printrejected myschema |  wc -l
235
# then repeat the same commands while looking at log file :
# smaller chunks will make it easier to "seek" for culprit files.
```

### Displaying schema statistics

Getting an history of all scans performed on a schema, use *filetree*:

```
$ filetree.py --printstats data1
action        endtime                     totalentries    skippedentries    newfiles    updatedfiles    deletedfiles
------------  --------------------------  --------------  ----------------  ----------  --------------  --------------
insert        2017-08-07 10:45:36.927532  350             0                 278         72              0
insert        2017-08-06 10:42:44.442179  367             0                 293         74              0
insert        2017-08-05 10:47:43.140992  371             0                 317         54              0
insert        2017-08-04 10:55:28.787931  334             0                 267         67              0
```

[DEPRECATED] Prints the date of latest insertion in the seedtree database :

```
$ seedtree5.py --getlastscandate myschema
2015-08-13 08:49:37
```

Getting statistics on the miniseed contents of a schema, use *seedtree*:

```
# without filtering gaps
$ seedtree5.py --printstats data1 
Database statistics
 +-- YP
     +-- CT01
     |   +-- 00
     |       +-- HHE
     |       |   +-- Gaps and/or overlaps : 6
     |       |   +-- Qualities and samples
     |       |   |   +-- quality M, 100.0Hz, 259 traces, 758763 records, 2181331486 samples
     |       |   +-- Data available from 2013-01-01T00:00:00 to 2013-09-10T11:59:59.990000
     |       +-- HHN
(...)
	
# display gaps only,
$ seedtree5.py --printstats --minratio 1.0  data1

# display overlaps only,
$ seedtree5.py --printstats --maxratio -1.0  data1
	
# selects a single station 
$ seedtree5.py --printstats myschema --network RD --station LOR
Database statistics
last insert on 2017-08-08 15:50
 +-- RD
     +-- LOR
         +-- 
             +-- BHE
             |   +-- Data available from 2013-08-06T14:57:33.580000 to 2017-07-17T23:59:59.980000
             |   +-- Qualities and samples
             |   |   +-- quality M, recordlength 4096, 50.0Hz, 2080 traces, 5506197520 samples, 2013-08-06 14:57 to 2017-07-17 23:59
             |   |   +-- quality D, recordlength 512, 50.0Hz, 240 traces, 100359181 samples, 2013-08-26 00:00 to 2013-11-18 00:00
             |   |   +-- quality D, recordlength 4096, 50.0Hz, 6 traces, 21533687 samples, 2014-05-07 12:22 to 2014-05-12 12:00
             |   +-- Gaps and/or overlaps : 1083
(...)
```

### Getting miniseed traces

Browse online help for all available *Search Parameters*. All search parameters are optional.

```
# all traces for network Z3 station A154A in schema myZ3
$ seedtree5.py --gettraces --network Z3 --station A154A myZ3
	
Index	Network.Station.Loc.Channel	Filename	Quality	Samplerate	Recordlength	Starttime → Endtime
21274	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.167	M	100.0	4096	2017-06-16 13:00:00 → 2017-06-16 23:59:59.990000
21275	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.168	M	100.0	4096	2017-06-17 00:00:00 → 2017-06-17 23:59:59.990000
21276	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.169	M	100.0	4096	2017-06-18 00:00:00 → 2017-06-18 23:59:59.990000
21277	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.170	M	100.0	4096	2017-06-19 00:00:00 → 2017-06-19 23:59:59.990000
21278	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.171	M	100.0	4096	2017-06-20 00:00:00 → 2017-06-20 23:59:59.990000
21279	Z3.A154A.00.HHE	/mnt/auto/archive/data/byyear/2017/Z3/A154A/HHE.D/Z3.A154A.00.HHE.D.2017.172	M	100.0	4096	2017-06-21 00:00:00 → 2017-06-21 23:59:59.990000
(...)

# using a start date
$ seedtree5.py --gettraces --network YP --station CT56 --channel HHZ --starttime 2013-09-01  yp2012
	
# using a time window
$ seedtree5.py --gettraces  --channel HHZ --station CT56 --starttime 2013-09-01T12:00:00 --endtime 2013-09-02T01:00:00  yp2012
```

### Geting gaps and overlaps

Without filtering, will display all gaps (and overlaps) :

```
$ seedtree5.py --getgaps --network Z3 --station A154A myZ3 
net    sta      loc  cha    qlty      Mhz  previous_endtime            current_starttime    julian    previous_basename           current_basename            time_delta         ratio
-----  -----  -----  -----  ------  -----  --------------------------  -------------------  --------  --------------------------  --------------------------  ---------------  -------
Z3     A154A     00  HHE    M-M       100  2017-06-24 23:59:59.990000  2017-06-25 01:00:00  175-176   Z3.A154A.00.HHE.D.2017.175  Z3.A154A.00.HHE.D.2017.176  +1:00:00.010000   360001
Z3     A154A     00  HHE    M-M       100  2017-07-14 23:59:59.990000  2017-07-15 02:00:00  195-196   Z3.A154A.00.HHE.D.2017.195  Z3.A154A.00.HHE.D.2017.196  +2:00:00.010000   720001
```

The output shows the characteristics of the *previous* and *current* miniseed trace where the gap occur. *time_delta* is the amount of time between both traces. For ratio definition, see *Terminology* section above.

With filtering:

```
# display all gaps,
$ seedtree5.py --getgaps --network YP --station CT53 --channel HHZ --minratio 1.0 mytest
	
# display overlaps only, and only for station CT53,
$ seedtree5.py --getgaps  --network YP --station CT53 --channel HHZ --maxratio -1.0 mytest
	
# display "large" gaps oly (longer than 100 time sampling frequency)
$ seedtree5.py --getgaps  --network YP --station CT53 --minratio 100 mytest
	
# display only "large" overlaps
$ seedtree5.py --getgaps  --channel HNZ --maxratio -20 mytest
```

### Getting data availability

All *search parameters* are optional. The output shows data availability time segments (see terminology section above).
 
 ```
$ seedtree5.py --getdataavailability --network RA --station OGAG --location 00 --channel HNZ mytest 

Network.Station.Loc.Channel	Starttime → Endtime
RA.OGAG.00.HNZ	2011-01-03 16:59:28.995510 → 2011-01-03 17:04:27.987510
RA.OGAG.00.HNZ	2011-01-06 09:47:43.995512 → 2011-01-06 09:52:42.987512
RA.OGAG.00.HNZ	2011-01-08 20:47:45.995508 → 2011-01-08 20:52:45.987508
RA.OGAG.00.HNZ	2011-01-08 22:19:56.995509 → 2011-01-08 22:24:55.987509
(...)
```

### Logging and monitoring code activity

The most detailed informations can be found into the log file. You may configure log verbosity in your configuration file. `Logging.INFO` is recommended for production systems. In case of problems, use `logging.DEBUG`.

```
# watch logs while seedtree is running.
# (you may grep on the PID to separate seedtree instance)
$ tail -f seedtree.log
	
# follow process activities and forks 
$ watch --interval=2  pstree -pacn seedtree
	
# or...
$ htop -d 20 -u seedtree
	
# for debugging, you may use strace, 
# though useful only for developpers 
$ strace -p 30325
Process 30325 attached
read(8, "13,147,09:03:57.457545\n14516224 "..., 28897) = 4096
read(8, ",147,09:09:58.785544\n14704640  M"..., 24801) = 4096
read(8, "47,09:15:50.089545\n14893056  MT_"..., 20705) = 4096
read(8, ",09:21:48.913545\n15081472  MT_RU"..., 16609) = 4096
read(8, "9:27:43.041544\n15269888  MT_RUI_"..., 12513) = 4096
fstat(8, {st_mode=S_IFIFO|0600, st_size=0, ...}) = 0
lseek(8, 0, SEEK_CUR)                   = -1 ESPIPE (Illegal seek)
...
```

## Using Python API

### Online documentation

Some methods embed a short online help, available from the Python prompt :

```		
>> import seedtree5
>> help (seedtree5)
>> ...
>> help mySeedtreeInstance.scan
```

From a shell prompt, use the `pydoc` command :

```
$ pydoc seedtree5
```

### Initializing a Seedtree object

Import the seedtree module then create an object pointing to the schema you want to work with. This object is the base for any query operations. 
 

```
>> import seedtree5
>> mytree = seedtree5.Seedtree( schema_name );
>> ...
```

#### Avoiding deadlocks

Creating a Seedtree object opens a connection to the database system. If the object is not destroyed at its end of life (or, in the case of an interactive Python session, is kept open indefinitely), connnection to the database and associated *locks* will not be released.

### Data types

Seedtree API returns either Python lists or [Named Tuples](https///docs.python.org/2/library/collections.html#collections.namedtuple).

### Scanning a set of files

Though possible from the Python API (see code for examples, especially the `main` sections), scan operations are ususally done from the shell prompt (see above section). Thus, Python API is documented for query operations only.

### Getting the most recent scan date

[DEPRECATED]

`get_last_scan_date()` returns a `datime` object for the date of the latest scan. The function returns `None` if no scan has happened yet.

	
```
>> mytree.get_last_scan_date()
datetime.datetime(2015, 8, 13, 8, 49, 37, 981930)
```

### Getting informations about seismic networks 

#### Network codes

Returns a list of all networks codes found in the schema.

```
>> import seedtree5
>> mytree = seedtree5.Seedtree( myschema )
...
>> mytree.get_networks()
['YP']
```

#### Stations codes

Returns stations included in a network.

```
>> mytree.get_stations('YP')
['CT01', 'CT02', 'CT55', 'CT56']
```

#### Stations codes (detailed)

Same as previous function, with extra informations about *location codes* and *channels*.

```
>> mytree.get_stations_details('YP')
[Record(station='CT01', location='00', channel='HHE'),
 Record(station='CT01', location='00', channel='HHN'),
...
 Record(station='CT56', location='00', channel='HHE'),
 Record(station='CT56', location='00', channel='HHN'),
 Record(station='CT56', location='00', channel='HHZ')]
```

#### Location codes

 
Returns a list of location codes found for a specific station.

```
>> mytree.get_location_codes('YP','CT55')
['00']
```

#### Channels

Returns a list of channels found for a specific location code.

```
>> mytree.get_channels('YP','CT55','00')
['HHE', 'HHN', 'HHZ']
```

#### Channels (all)

Same as previous function, but takes no argument.

```
>> mytree.get_all_channels()
[Record(network='YP', station='CT01', location='00', channel='HHE'),
 Record(network='YP', station='CT01', location='00', channel='HHN'),
 Record(network='YP', station='CT01', location='00', channel='HHZ'),
...
 Record(network='YP', station='CT56', location='00', channel='HHZ')]
```

#### Qualities, record length and samples overview 

For a specific channel, returns all distinct quality flags found, record length, sampling rates, number of traces, number of miniseed records, number of samples, earliest start time, and latest end time. Output is sorted by start time and end time.


```
>> mytree.get_qualities_and_samples('RD','LAR','00','BHZ')
[ Record(quality='M', samplerate=50.0, recordlength=4096, totaltraces=2075L, totalsamples=5514718591L, starttime=datetime.datetime(2013, 8, 6, 14, 57, 32, 680000), endtime=datetime.datetime(2017, 7, 17, 23, 59, 59, 980000)),
 Record(quality='D', samplerate=50.0, recordlength=512, totaltraces=224L, totalsamples=101223647L, starttime=datetime.datetime(2013, 8, 26, 0, 0, 4, 260000), endtime=datetime.datetime(2013, 11, 18, 0, 0, 8, 560000)),
 Record(quality='D', samplerate=50.0, recordlength=4096, totaltraces=6L, totalsamples=21534055L, starttime=datetime.datetime(2014, 5, 7, 12, 22, 10), endtime=datetime.datetime(2014, 5, 12, 12, 0, 11, 80000))
]
```

### Getting miniseed traces

See terminology section above for trace definition.
           
The number of traces for a single channel can be quite huge : querying the database may involve significant access times or risks of memory saturation on client side. To avoid this problem, traces are accessed using [database cursors](http://www.postgresql.org/docs/9.3/static/plpgsql-cursors.html). Usage is the same as reading a file :

 1.  open a *handler* (with optional search parameters)
 2.  make successive iterations (read operation), getting traces one by one
 3.  close the *handler*

The following loops displays all traces for channel `1A.CORRE.00.BHZ` : 

```
# opens the handler with search parameters 
>> mytree.open_traces(network='1A',station='CORRE',location='00',channel='BHZ')
`<unnamed portal 1>`

# iterate on traces
>> for trace in mytree: print trace

Record(fileindex=1417, traceindex=1465, dirname='/mnt/auto/archive/data/byyear/2010/1A/CORRE/BHZ.D', basename='1A.CORRE.00.BHZ.D.2010.252', filesize=3932160L, datemodified=1359539971000000L, lastupdated=datetime.datetime(2017, 7, 11, 13, 42, 16, 170998), network='1A', station='CORRE', location='00', channel='BHZ', quality='M', samplerate=40.0, recordlength=4096, fileoffset=0L, nsamples=3456000, starttime=datetime.datetime(2010, 9, 9, 0, 0), endtime=datetime.datetime(2010, 9, 9, 23, 59, 59, 975000))
Record(fileindex=1418, traceindex=1466, dirname='/mnt/auto/archive/data/byyear/2010/1A/CORRE/BHZ.D', basename='1A.CORRE.00.BHZ.D.2010.253', filesize=3796992L, datemodified=1359539963000000L, lastupdated=datetime.datetime(2017, 7, 11, 13, 42, 16, 171037), network='1A', station='CORRE', location='00', channel='BHZ', quality='M', samplerate=40.0, recordlength=4096, fileoffset=0L, nsamples=3337542, starttime=datetime.datetime(2010, 9, 10, 0, 0), endtime=datetime.datetime(2010, 9, 10, 23, 10, 38, 525000))
...
	
# close the handler
>> mytree.close_traces()
	
# other examples using open_traces():

# traces for network Y9
>> mytree.open_traces(network = 'Y9') 
>> ...
	
# traces filtered by starttime and endtime
>> mytree.open_traces(network='Y9', station='AMAC0', location='00', channel='EHZ', starttime='2009-08-21 00:01:00', endtime='2009-08-21 02:10:00')
>> ...
	 
# traces filtered by sampling rate filter 
>> mytree.open_traces(network='Y9',samplerate=50)
>> ...
```

After a call to `open_traces`, subsequent calls on the `Seedtree` class iterator (see example above) returns the next trace as a *NamedTuple* (see Python documentation). Each field contains information on the trace. Every parameters are optional. A missing parameter or equal to `None` means *any*. 

```
open_traces ( 
    network    (string)
    station    (string)
    location   (string)
    channel    (string)
    starttime  (date, see format details below)
    endtime    (date, see format details below)
    samplerate (flottant)
    quality    (character)
    recordlength  (integer)
    )
```

*  `starttime` queries for traces holding data after this date 
*  `endtime` queries for traces containing data before this date 
*  traces ares sorted by `network`, `station`, `location`, `channel`, `starttime`, `endtime`, `quality`, `recordlength`
*  `samplerate` queries traces with such *sampling rate*
*  `quality` queries for traces with such SEED quality flag 

The value returned by `open_traces()` is currently not used further on. It is recommended to store it, anticipating evolutions of the API.

It is recommended to name all the parameters when calling `open_traces`, this will facilitate adaptations of your code for latter versions of the API.

#### Date formats

`starttime` and `endtime` parameters are passed to PostgreSQL without alteration. Search is performed on *timestamp* field. PostgreSQL is rather liberal on timestamp formats : see documentation for [allowable formats](http://www.postgresql.org/docs/current/static datatype-datetime.html). The following formats are recommended: 

```
YYYY-MM-DD HH:MM:SS (eg. "1999-01-08 04:05:06")

or

YYYY-MM-DD HH:MM
```

#### Trace : tuple structure

Each iteration made after a call to `open_traces()` returns a tuple with attributes readable by your application. These attributes are extracted from the values provided by *IRIS miniseed inspector* (*msi*). For more details, see *msi* documentation.

This list might not be exhaustive : explore tuple contents by hand.

Attribute name | Comment
-------------- | -------
fileindex      | Internal. Do not use.
traceindex     | Internal. Do not use.
dirname        | path of the directory containing the file
basename       | basename of file containing the trace
filesize       | size of file
datemodified   | file modification time (Unix epoch time)
lastupdated    | date this file was last inserted/modified into the database
network        | SEED network code
station        | SEED station code
location       | SEED location code
channel        | SEED channel code
quality        | SEED data quality flag
samplerate     | SEED sampling frequency
recordlength   | SEED record length
fileoffset     | trace position is file, as offset in *bytes*
nsamples       | number of samples in trace
starttime      | start time of data
endtime        | end time of data

### Getting first or last trace datetime

For a specific *network*, *station*, *location code* et *channel*, the `get_channel_mintime()` function (respectively `get_channel_maxtime()`) returns the start date of the 1st trace (respectively the date for the last trace). The `startDate` parameter (respectively `endDate`) is optional  : it returns the starting date of the 1st trace available *after* (respectively *before*) a given date. If `startDate` (respectively `endDate`) is included in a time range covered by a trace, then `startDate` is returned (respectively `endDate`).  

```
get_channel_mintime ( network, station, location, channel, startDate = None )
get_channel_maxtime ( network, station, location, channel, endDate = None )
```

Dates are formatted using the same format as for the `open_traces()` function (see examples above). 

```
>> print mytree.get_channel_mintime('FR','ANTF','00','HHZ',None)
2011-01-01 00:00:00.838300+00:00
	
>> print mytree.get_channel_mintime('FR','ANTF','00','HHZ','2011-05-01')
2011-05-01 00:00:00.804300+00:00
	
>> print mytree.get_channel_mintime('FR','ANTF','00','HHZ','2015-01-01')
None
	
>> print mytree.get_channel_maxtime('FR','ANTF','00','HHZ','2015-01-01 10:00:00')
2012-01-01 00:00:00.002300+00:00
```

### Getting gaps (and overlaps)

The `get_gaps()` function returns a list of *gaps*, ie. a couple of traces (called *previous* trace et *current* trace) whose association corresponds to the qualification of *gap* (or overlap). The *previous* trace is the trace where the gap starts and the *current* trace is the trace where the gap ends.

All parameters are optional. A missing parameter (or `None`) means "any". Gaps are sorted by `previous_endtime` and `current_starttime`. By default, `minratio` and `maxratio` are set to `None`. `Radiodelta` attribute makes it possible to do your own filtering in your application. For *ratio* definition, see terminology section above.

```	
get_gaps (
  network,     
  station,   
  location,   
  channel,    
  minratio     -- select gaps and overlaps with ratio greater or equal to minratio
  maxratio     -- select gaps and overlaps with ratio lower or equal to maxratio
  startDate,   -- gaps must start after...
  endDate      -- gaps must end before...  
  quality      -- outputs gaps with such quality 
  )
```


`get_gaps()` returns a list of Python named tuples, with the following attributes :

Attribute name|Comment
------------|-------------
network|SEED identifier
station|SEED identifier
location|SEED identifier
channel|SEED identifier
gap_index|internal, do not use
previous_trace_index|internal, do not use
current_trace_index|internal, do not use
previous_endtime|end date of the previous trace (otherwise said : start date of the gap) 
current_starttime|start date of the current trace (otherwise said : end date of the gap) 
previous_dirname|directory path of the previous trace
previous_basename|filename containing the previous trace
current_dirname|directory path of the current trace 
current_basename|filename containing the current trace
previous_quality|quality of the previous trace
current_quality|quality of the current trace
expecteddelta| expected time difference (in seconds) between *current_starttime* et *previous_endtime* (calculated according to *sample rate*) 
truedelta|time difference observed (in second) between *current_starttime* and *previous_endtime*. This is the effective duration of the gap or overlap.
ratiodelta|ratio between *truedelta* and *expecteddelta*

#### Example usages

Note : it is recommended to name all parameters when calling the `get_gaps()` function.

Getting all gaps for station PC01:

```
>> mytree = seedtree5.Seedtree('my_schema')
>> gaps = mytree.get_gaps(network='X7',station='PC01');
```

Number of gaps:
```
>> print len(gaps)
6
```

Print first gap:

```
>> print gaps[0]
Record(gapindex=876449, network='X7', station='PC01', location='00', channel='HHE', previous_traceindex=6371507, current_traceindex=6371499, previous_endtime=datetime.datetime(2014, 1, 8, 23, 59, 59, 990000), current_starttime=datetime.datetime(2014, 1, 9, 8, 28, 26, 100000), previous_dirname='/mnt/auto/archive/data/bynet/X72010/2014/PC01/HHE.D', previous_basename='X7.PC01.00.HHE.D.2014.008', current_dirname='/mnt/auto/archive/data/bynet/X72010/2014/PC01/HHE.D', current_basename='X7.PC01.00.HHE.D.2014.009', previous_quality='M', current_quality='M', expecteddelta=datetime.timedelta(0, 0, 10000), truedelta=datetime.timedelta(0, 30506, 110000), ratiodelta=3050610.0)
```

Selects all gaps with length greater than 10x the sampling frequency :
```
>> gaps = mytree.get_gaps(minratio=10.0);
>> print gaps[0]
Record(gapindex=979187, network='X7', station='PY4A', location='00', channel='HHN',
previous_traceindex=309560, current_traceindex=309561, previous_endtime=datetime.datetime(2010, 9, 15, 14, 1, 48, 958393), current_starttime=datetime.datetime(2010, 9, 15, 14, 1, 51, 968391), 
previous_dirname='/mnt/auto/archive/data/bynet/X72010/2010/PY4A/HHN.D', previous_basename='X7.PY4A.00.HHN.D.2010.258', current_dirname='/mnt/auto/archive/data/bynet/X72010/2010/PY4A/HHN.D', current_basename='X7.PY4A.00.HHN.D.2010.258', 
previous_quality='M', current_quality='M', expecteddelta=datetime.timedelta(0, 0, 10000), truedelta=datetime.timedelta(0, 3, 9998), ratiodelta=301.0)
```

Selects overlaps with duration greater than 10 times sampling frequency:
```
>> overlaps = f.get_gaps(minratio=-10.0)
```

### Getting data availability

`get_dataavailability` function makes it possible to get data availability (continuous data) as described at the beginning of the document (see Terminology section). Each parameter is optionnal (`None` means "any").  The boolean *gaps* (//False// by default) reverses the results : if *True*, the function returns aggregated gaps instead of aggregated traces.

Get the first data availability range for RA.OGAG.00.HNZ:
```
>> data = mytree.get_dataavailability('RA', 'OGAG', '00','HNZ', None, None, gaps=False)
>> print data[0]
Record ( network='RA', station='OGAG', location='00', channel='HNZ', starttime=datetime.datetime(2011, 1, 3, 16, 59, 28, 995510), endtime=datetime.datetime(2011, 1, 3, 17, 4, 27, 987510) )
```

Get all data availability for station PORMAL, channel BHZ:

```
>> mytree.get_dataavailability(station='PORMA', channel='BHZ')
[Record(network='1A', station='PORMA', location='00', channel='BHZ', starttime=datetime.datetime(2009, 10, 29, 2, 41, 26, 80000), endtime=datetime.datetime(2009, 10, 29, 2, 45, 22, 840000)),
 Record(network='1A', station='PORMA', location='00', channel='BHZ', starttime=datetime.datetime(2009, 10, 29, 2, 45, 28, 625000), endtime=datetime.datetime(2009, 10, 29, 3, 11, 25, 850000)),
 Record(network='1A', station='PORMA', location='00', channel='BHZ', starttime=datetime.datetime(2009, 11, 4, 3, 49, 1, 550000), endtime=datetime.datetime(2009, 12, 31, 23, 59, 59, 975000)),
 Record(network='1A', station='PORMA', location='00', channel='BHZ', starttime=datetime.datetime(2010, 1, 1, 0, 0), endtime=datetime.datetime(2010, 3, 28, 4, 58, 11, 750000)),
(...)
 Record(network='1A', station='PORMA', location='00', channel='BHZ', starttime=datetime.datetime(2012, 1, 1, 0, 0), endtime=datetime.datetime(2012, 2, 5, 7, 42, 19, 650000))]
```

Get first gap for RA.OGAG.00.HNZ :
```
>> data = mytree.get_dataavailability('RA', 'OGAG', '00','HNZ', None, None, gaps=True)
>> print data[0]
Record(network='RA', station='OGAG', location='00', channel='HNZ', starttime=datetime.datetime(2011, 1, 3, 17, 4, 27, 987510), endtime=datetime.datetime(2011, 1, 6, 9, 47, 43, 995512))
```

### Closing a Seedtree object

Whenever a Seedtree object is not used anymore, call its `close()` method to ensure some ressources are released (as database connections or locks).  After calling, the object is not usable anymore. 

```	
>> import seedtree5
>> mytree = seedtree5.Seedtree ( ... )
...
...
# end of object usage
>> mytree.close()
```

## Using the PL/pgSQL API

Many of the Python API functions are passed to their PL/pgSQL counterpart. See `src/schema_init.sql` after the *"Seedtree API"* string for details.

## Bugs, limitations

#### Database locks

A seedtree command opens a connection on the database system : if this command gets locked by a *pipe*, example:  `seedtree5.py --printrejected data1 | less`), connection to the database and *locks* might not be released, resulting, in some circumstances, in a database lock. Mixing "blocking pipes" with *seedtree* commands is not recommended on production systems. 

The same behaviour may happen when using Seedtree from the *Python* interactive prompt : do not forget to close your Seedtree object or to exit Python prompt when finished.

## Misc tools

Various utilities may be found in the `src/` directory. See README file. 

### readmseed.py

This is the script used by *Seedtree* to read miniseed files. It is based on *IRIS msi*. It is possible to call the script from the command line to check traces generated for each miniseed file. Example:

```
$ readmseed.py --bulktraces /mntarchive/data/bynet/FR/2006/ANTF/BHE.D/FR.ANTF.00.BHE.D.2006.291
```

First displays the number of traces found in the file,

```
[DEBUG] +1084 traces in /mnt/archive/data/bynet/FR/2006/ANTF/BHE.D/FR.ANTF.00.BHE.D.2006.291
```

Then displays a dump of all traces, column-formatted,

```
fileindex network station location channel quality starttime endtime nsamples samplerate recordlength fileoffset
n/a	FR	ANTF	00	BHE	D	2006-10-18T00:00:00.027900	2006-10-18T00:01:58.307900	2958	25	4096	0
n/a	FR	ANTF	00	BHE	D	2006-10-18T00:02:02.443900	2006-10-18T00:03:06.923900	1613	25	4096	4096
n/a	FR	ANTF	00	BHE	D	2006-10-18T00:03:10.019900	2006-10-18T00:03:19.259900	232	25	4096	8192
n/a	FR	ANTF	00	BHE	D	2006-10-18T00:03:25.403900	2006-10-18T00:05:09.923900	2614	25	4096	12288
(...)
```

### msimetadata2json.sh

This script extracts and factorize informations from miniseed headers, output in JSON format. This tool is not used in current version of *Seedtree*. You may used *Obspy* or *EIDA WFcatalog* instead.

### RESIF datacentre toolbox

Misc script used at RESIF.

*  `seedtree_fpart_scan.sh` :  makes partitions out of a large set of files and prints corresponding Seedtree scan commands. Might not be useful anymore, but who knows.
*  `bynet_to_byyear.py` : converts *by-net* paths to *by-year* paths. 
*  `initialize_all_schemas.sh` : prints commands for initializing one database schema per network found into Station webservice. 

