-- -----------------------------------------------------
-- Filetree and Seedtree database creation files
-- beware : this will delete any existing table
--
-- This file gathers commands to create Filetree and Seedtree database objects.
-- Filetree has its own separated data model, but Seedtree objects are built on top of it.
-- -----------------------------------------------------

-- CREATE LANGUAGE IF NOT EXISTS plpgsql;

-- #####################################################
-- Filetree 
-- #####################################################

CREATE SCHEMA :newschema ;
COMMENT ON SCHEMA :newschema IS :commentschema ;

SET search_path TO :newschema;

-- meta

CREATE TABLE meta ( database_version float DEFAULT 2017.166, creationtime timestamp DEFAULT now() );
INSERT INTO meta DEFAULT VALUES;
COMMENT ON TABLE meta IS 'informations about this Seedtree schema instance';

-- files

CREATE TABLE files (
  fileindex SERIAL PRIMARY KEY,
  dirname text NOT NULL,
  basename text NOT NULL,
  filesize bigint NOT NULL,
  datemodified bigint NOT NULL,
  checksum text,
  mimetype text,
  lastupdated timestamp DEFAULT clock_timestamp(),
  UNIQUE (dirname, basename)
);
COMMENT ON TABLE files IS 'indexed files';

CREATE INDEX files_index1 ON files ( dirname, basename );
CREATE INDEX files_index2 ON files ( checksum );
CREATE INDEX files_index3 ON files ( datemodified );
CREATE INDEX files_index4 ON files ( mimetype );

-- journal

CREATE TYPE eventtype AS ENUM ('create','delete', 'update');

CREATE TABLE journal (
    journalstamp timestamp NOT NULL,
    fileindex bigint NOT NULL,
    dirname text,
    basename text,
    event eventtype NOT NULL
);
COMMENT ON TABLE journal IS 'journal of updates made by Filetree';
CREATE INDEX journal_index1 ON journal ( fileindex );


-- logs

DROP TABLE IF EXISTS logs CASCADE;
CREATE TABLE logs (
  action text,
  starttime timestamp,
  endtime timestamp,
  fromhost text,
  totalentries bigint,
  skippedentries bigint,
  newfiles bigint,
  updatedfiles bigint,
  deletedfiles bigint
  );
  
COMMENT ON TABLE logs IS 'logs about indexing operations';

-- returns latest scan date
CREATE OR REPLACE FUNCTION get_last_scan_date() RETURNS timestamp AS $$
    SELECT max(endtime) FROM logs WHERE action='insert';
$$ LANGUAGE SQL;

-- view files with duplicate contents
CREATE OR REPLACE VIEW files_duplicate_content AS
    SELECT checksum,count(checksum) FROM files GROUP BY checksum
    HAVING count (checksum) > 1 ;
COMMENT ON VIEW files_duplicate_content IS 'count files that have the same content (ie. similar MD5 sum)';

-- view file with duplicate names 
CREATE OR REPLACE VIEW files_duplicate_name AS
  SELECT basename,count(basename) FROM files 
  GROUP BY basename HAVING COUNT (basename) > 1 ;
COMMENT ON VIEW files_duplicate_name IS 'count files that have the same name';

-- duplicate file names with different content
CREATE OR REPLACE VIEW files_duplicate_name_with_different_content AS
  SELECT basename,count(basename) FROM ( 
    SELECT basename FROM ( 
      SELECT basename, checksum, count(checksum) FROM files WHERE basename IN ( 
	SELECT basename FROM files_duplicate_name ) GROUP BY basename,checksum ORDER BY basename 
	) AS table1
      ) AS table2 
  GROUP BY basename HAVING COUNT (basename)>1;
COMMENT ON VIEW files_duplicate_name_with_different_content IS 'count the occurences of files that have the same name but different content';

-- File names with duplicate content
-- SELECT  dirname,basename,checksum FROM files WHERE checksum IN (SELECT checksum FROM files_duplicate_content) ORDER BY checksum;

-- select directory_index,relative_path,basename,checksum from files where basename in ( select basename from files_duplicate_name_with_different_content) order by directory_index,basename;
-- SELECT basename,checksum,count(checksum) FROM files WHERE basename IN ( SELECT basename FROM files_duplicate_name )  GROUP BY basename,checksum ORDER BY basename;

-- get file by path   
CREATE OR REPLACE FUNCTION file_search_occurence ( file_path text, file_name text ) 
  RETURNS files AS $$
    SELECT * FROM files WHERE files.dirname=file_path AND files.basename=file_name;
$$ LANGUAGE SQL;

-- ##########################################################
-- Seedtree
-- ##########################################################

-- ---------------------------------------------------------------
-- Traces 
-- a trace is a group of time-contiguous miniseed records with similar metadata  
-- note : one trace does not cross miniseed file boundary it belongs to,
-- for this look at "integrated traces" below.
-- ---------------------------------------------------------------

DROP TABLE IF EXISTS traces CASCADE;
CREATE TABLE traces (
  traceindex SERIAL PRIMARY KEY,
  fileindex integer REFERENCES files ON DELETE CASCADE ON UPDATE CASCADE,
  network text NOT NULL,
  station text NOT NULL,
  location text,
  channel text NOT NULL,
  quality char(1) NOT NULL CHECK (quality IN ('D','R','M','Q')),
  starttime timestamp NOT NULL,
  endtime timestamp NOT NULL,
  nsamples integer NOT NULL,
  samplerate real NOT NULL,
  recordlength integer,
  fileoffset bigint,
  updated boolean DEFAULT TRUE
);

COMMENT ON TABLE traces IS 'miniseed traces by file';
COMMENT ON COLUMN traces.starttime IS 'start time of first record in the trace';
COMMENT ON COLUMN traces.starttime IS 'end time of last record in the trace';
COMMENT ON COLUMN traces.nsamples IS 'number of samples in the trace';
COMMENT ON COLUMN traces.samplerate IS 'nominal sample rate (calculated from sample rate factor and sample rate multiplier)';
COMMENT ON COLUMN traces.recordlength IS 'record length (512, 4096, ...)';
COMMENT ON COLUMN traces.fileoffset IS 'beginning position of trace, as a byte offset in file';
COMMENT ON COLUMN traces.updated IS 'trace has been updated since previous scan';

-- create indexes on Traces table
CREATE OR REPLACE FUNCTION create_index_on_traces() RETURNS void AS $$
    CREATE INDEX traces_index1 ON traces ( fileindex );
    CREATE INDEX traces_index2 ON traces ( network );
    CREATE INDEX traces_index3 ON traces ( station ); 
    CREATE INDEX traces_index4 ON traces ( location ); 
    CREATE INDEX traces_index5 ON traces ( channel ); 
    CREATE INDEX traces_index6 ON traces ( starttime ); 
    CREATE INDEX traces_index7 ON traces ( endtime ); 
    CREATE INDEX traces_index8 ON traces ( recordlength ); 
    CREATE INDEX traces_index9 ON traces ( updated ); 
$$ LANGUAGE SQL;

-- drop indexes on Traces table
-- warning : this will Exclusive-Lock Traces table until commit happens
CREATE OR REPLACE FUNCTION drop_index_on_traces() RETURNS void AS $$
    DROP INDEX IF EXISTS traces_index1 CASCADE; 
    DROP INDEX IF EXISTS traces_index2 CASCADE; 
    DROP INDEX IF EXISTS traces_index3 CASCADE; 
    DROP INDEX IF EXISTS traces_index4 CASCADE; 
    DROP INDEX IF EXISTS traces_index5 CASCADE; 
    DROP INDEX IF EXISTS traces_index6 CASCADE; 
    DROP INDEX IF EXISTS traces_index7 CASCADE; 
    DROP INDEX IF EXISTS traces_index8 CASCADE; 
    DROP INDEX IF EXISTS traces_index9 CASCADE; 
$$ LANGUAGE SQL;

SELECT create_index_on_traces();

-- on file update (ie. checksum or size modified), delete corresponding traces
--CREATE OR REPLACE FUNCTION delete_traces_when_file_updated() RETURNS TRIGGER AS $$
--  BEGIN  
--    EXECUTE 'DELETE FROM traces WHERE fileindex=' || OLD.fileindex;
--    RETURN OLD;
--  END;
--$$ LANGUAGE plpgsql;

--CREATE TRIGGER auto_delete_traces AFTER UPDATE OF checksum,filesize ON files
--  FOR EACH ROW EXECUTE PROCEDURE delete_traces_when_file_updated();

--
-- materialized views on Traces
--

-- networks 

CREATE MATERIALIZED VIEW networks AS 
  SELECT network FROM traces
  GROUP BY network 
  ORDER BY network;
COMMENT ON MATERIALIZED VIEW networks IS 'available networks';

-- stations 

CREATE MATERIALIZED VIEW stations AS
  SELECT network,station FROM traces 
  GROUP BY network,station 
  ORDER BY network,station;
COMMENT ON MATERIALIZED VIEW stations IS 'available stations among all networks';

-- locations 

CREATE MATERIALIZED VIEW location_codes AS
  SELECT network,station,location FROM traces 
  GROUP BY network,station,location 
  ORDER BY network,station,location;
COMMENT ON MATERIALIZED VIEW location_codes IS 'available location codes among all stations';

-- channels 

CREATE MATERIALIZED VIEW channels AS
  SELECT network,station,location,channel FROM traces 
  GROUP BY network,station,location,channel 
  ORDER BY network,station,location,channel;
COMMENT ON MATERIALIZED VIEW channels IS 'available channels among all location codes';

-- qualities 

CREATE MATERIALIZED VIEW qualities AS
  SELECT network,station,location,channel,quality FROM traces 
  GROUP BY network,station,location,channel,quality 
  ORDER BY network,station,location,channel,quality;
COMMENT ON MATERIALIZED VIEW qualities IS 'available qualities among all channels';

-- updated channels

CREATE MATERIALIZED VIEW updated_channels AS
    SELECT network,station,location,channel,quality,
        min(starttime) AS starttime,max(endtime) AS endtime, 
        EXTRACT(year FROM min(starttime)) AS updated_from_year, 
        EXTRACT(year FROM max(endtime)) AS updated_to_year 
    FROM traces 
    WHERE updated 
    GROUP BY network,station,location,channel,quality
    ORDER BY network,station,location,channel,quality, starttime, endtime;
COMMENT ON MATERIALIZED VIEW updated_channels IS 'channels updated during previous scan, with timespan of updates';

-- refresh views
CREATE OR REPLACE FUNCTION refresh_traces_views() RETURNS void AS $$
    REFRESH MATERIALIZED VIEW networks;
    REFRESH MATERIALIZED VIEW stations;
    REFRESH MATERIALIZED VIEW location_codes;
    REFRESH MATERIALIZED VIEW channels;
    REFRESH MATERIALIZED VIEW qualities; 
    REFRESH MATERIALIZED VIEW updated_channels; 
$$ LANGUAGE SQL;

-- ---------------------------------------------------------------
-- Data availability
-- Data availability is a set of time windows for which a channel has some data available,
-- whatever is its quality flag. Data availability is computed from Traces table. 
-- ---------------------------------------------------------------

CREATE TYPE availabilitytype AS ENUM ('data','gap');

DROP TABLE IF EXISTS dataavailability CASCADE;
CREATE TABLE dataavailability (
  dataavailabilityindex SERIAL PRIMARY KEY,
  network text NOT NULL,
  station text NOT NULL,
  location text,
  channel text NOT NULL,
  starttime timestamp NOT NULL,
  endtime timestamp NOT NULL,
  type availabilitytype NOT NULL,
  UNIQUE (network, station, location, channel, starttime, endtime)
);

COMMENT ON COLUMN dataavailability.starttime IS 'start time of dataavailability or gap';
COMMENT ON COLUMN dataavailability.endtime IS 'end time of dataavailability or gap';
COMMENT ON COLUMN dataavailability.type IS 'time window is either a data availability or a gap (ie. data non-availability)';

CREATE INDEX dataavailability_index1 ON dataavailability ( network );
CREATE INDEX dataavailability_index2 ON dataavailability ( station ); 
CREATE INDEX dataavailability_index3 ON dataavailability ( location ); 
CREATE INDEX dataavailability_index4 ON dataavailability ( channel ); 
CREATE INDEX dataavailability_index5 ON dataavailability ( starttime ); 
CREATE INDEX dataavailability_index6 ON dataavailability ( endtime ); 
CREATE INDEX dataavailability_index7 ON dataavailability ( type ); 

-- --------------------------
-- Rejected miniseed files
-- --------------------------

DROP TABLE IF EXISTS rejectedminiseedfiles CASCADE;
CREATE TABLE rejectedminiseedfiles (
  rejectedindex SERIAL PRIMARY KEY,
  fileindex integer REFERENCES files ON DELETE CASCADE ON UPDATE CASCADE,
  comment text DEFAULT NULL,
  UNIQUE (fileindex)
);

COMMENT ON TABLE rejectedminiseedfiles IS 'miniseed files rejected by Seedtree';
CREATE INDEX rejectedminiseedfiles_index1 ON rejectedminiseedfiles ( fileindex );

-- ---------------------------------------------------------------
-- Metadata details from miniseed files,
-- gathers synthetic information about miniseed metadata found in a file 
-- ---------------------------------------------------------------

-- feature not used in this version
-- DROP TABLE IF EXISTS miniseedfilesmetadata CASCADE;
-- CREATE TABLE miniseedfilesmetadata (
--  metadataindex SERIAL PRIMARY KEY,
--  fileindex integer REFERENCES files ON DELETE CASCADE ON UPDATE CASCADE,
--  details json DEFAULT NULL,
--  UNIQUE (fileindex)
-- );

-- COMMENT ON TABLE miniseedfilesmetadata IS 'synthetic information about miniseed headers found in a file';
-- CREATE INDEX miniseedfilesmetadata_index1 ON miniseedfilesmetadata ( fileindex );

--------------------------------------------------------------------
-- Gaps and Overlaps 
-- each row is a gap (or overlap) between two traces
--------------------------------------------------------------------
DROP TABLE IF EXISTS gaps CASCADE;
CREATE TABLE gaps (
  gapindex serial PRIMARY KEY,
  network text NOT NULL,
  station text NOT NULL,
  location text NOT NULL,
  channel text NOT NULL,
  previous_traceindex integer REFERENCES traces ON DELETE CASCADE ON UPDATE CASCADE,
  current_traceindex integer REFERENCES traces ON DELETE CASCADE ON UPDATE CASCADE,
  previous_endtime timestamp, 
  current_starttime timestamp,
  previous_dirname text, 
  previous_basename text, 
  current_dirname text, 
  current_basename text,
  previous_quality char(1) NOT NULL,
  current_quality char(1) NOT NULL,
  expecteddelta interval NOT NULL,
  truedelta interval NOT NULL, 
  ratiodelta real NOT NULL
);
COMMENT ON TABLE gaps IS 'gaps and overlaps calculated for each channel';
COMMENT ON COLUMN gaps.ratiodelta IS '= equals (truedelta % expecteddelta). This ratio is < 1.0 if overlap, and > 1.0 if gap';

CREATE INDEX gaps_index1 ON gaps ( network );
CREATE INDEX gaps_index2 ON gaps ( station );
CREATE INDEX gaps_index3 ON gaps ( location );
CREATE INDEX gaps_index4 ON gaps ( channel );
CREATE INDEX gaps_index5 ON gaps ( truedelta );
CREATE INDEX gaps_index6 ON gaps ( ratiodelta );
CREATE INDEX gaps_index7 ON gaps ( previous_traceindex );
CREATE INDEX gaps_index8 ON gaps ( current_traceindex );

-- delete obsolete products
-- delete any products that has overlapping dates with updated_channels date ranges.
-- EDIT function replaced by delete_obsolete_products_by_year()
--CREATE OR REPLACE FUNCTION delete_obsolete_products() RETURNS VOID AS $$
--    DELETE FROM gaps 
--    WHERE EXISTS ( SELECT * FROM updated_channels WHERE 
--        gaps.network = updated_channels.network AND gaps.station = updated_channels.station
--        AND gaps.location = updated_channels.location AND gaps.channel = updated_channels.channel
--        AND (gaps.previous_endtime,gaps.current_starttime) OVERLAPS (updated_channels.starttime,updated_channels.endtime)
--        );
--    DELETE FROM dataavailability
--    WHERE EXISTS ( SELECT * FROM updated_channels WHERE 
--        dataavailability.network = updated_channels.network AND dataavailability.station = updated_channels.station
--        AND dataavailability.location = updated_channels.location AND dataavailability.channel = updated_channels.channel );
--$$ LANGUAGE SQL;

-- delete products by year 
CREATE OR REPLACE FUNCTION delete_obsolete_products_by_year (net text, sta text, loc text, chan text, qual text, y integer) RETURNS VOID AS $$
    DELETE FROM gaps 
    WHERE (  
        gaps.network = net AND gaps.station = sta AND gaps.location = loc AND gaps.channel = chan AND gaps.current_quality = qual 
        AND EXTRACT ( year FROM gaps.current_starttime) = y 
        );
    DELETE FROM dataavailability
    WHERE dataavailability.network = net AND dataavailability.station = sta AND dataavailability.location = loc AND dataavailability.channel = chan  
        AND EXTRACT ( year FROM dataavailability.starttime) = y 
$$ LANGUAGE SQL;

-- -----------------------------------------------------------------
-- Seedtree API functions
-- -----------------------------------------------------------------

-- all networks in current schema
CREATE OR REPLACE FUNCTION get_networks() RETURNS SETOF text AS $$
    SELECT network AS network FROM networks;
$$ LANGUAGE SQL;

-- all stations for a given network
CREATE OR REPLACE FUNCTION get_stations ( nw text ) RETURNS SETOF text AS $$
    SELECT DISTINCT station FROM stations WHERE stations.network = nw;
$$ LANGUAGE SQL;

-- all stations for a given network
-- (same as above function, but with more details)
CREATE OR REPLACE FUNCTION get_stations_details ( nw text ) RETURNS TABLE (station text, location text, channel text) AS $$
    SELECT station, location, channel FROM channels
    WHERE channels.network = nw;
$$ LANGUAGE SQL;

-- all location codes for a station
CREATE OR REPLACE FUNCTION get_location_codes ( nw text, stn text ) RETURNS SETOF text AS $$
    SELECT DISTINCT location_codes.location AS location FROM location_codes
    WHERE location_codes.network = nw
    AND location_codes.station = stn;
$$ LANGUAGE SQL;

-- all channels for a station
CREATE OR REPLACE FUNCTION get_channels ( nw text, stn text, loc text ) RETURNS SETOF text AS $$
    SELECT DISTINCT channels.channel FROM channels 
    WHERE channels.network = nw
    AND channels.station = stn AND channels.location = loc;
$$ LANGUAGE SQL;

-- all channels in current schema 
CREATE OR REPLACE FUNCTION get_all_channels () RETURNS SETOF channels AS $$
    SELECT network,station,location,channel FROM channels
$$ LANGUAGE SQL;

-- qualities, recordlength, and samplerates for a single channel
-- FIXME devrait être remplacé par vue matérialisée (cf. vue qualities) ?
CREATE OR REPLACE FUNCTION get_qualities_and_samples ( nw text, stn text, loc text, chn text ) 
    RETURNS TABLE ( quality char(1), samplerate real, recordlength integer , totaltraces bigint, totalsamples bigint, starttime timestamp, endtime timestamp ) AS $$
    SELECT quality,samplerate,recordlength, count(*) AS totaltraces, sum(nsamples) AS totalsamples,min(starttime) AS starttime,max(endtime) AS endtime 
    FROM traces 
    WHERE network = nw AND station = stn AND location = loc AND channel = chn
    GROUP BY quality,recordlength,samplerate ORDER BY starttime,endtime;
$$ LANGUAGE SQL;

-- all updated channels (including quality column)
CREATE OR REPLACE FUNCTION get_updated_channels_including_quality() RETURNS SETOF updated_channels AS $$
    SELECT * FROM updated_channels ORDER BY network,station,location,channel;
$$ LANGUAGE SQL;

-- all updated channels (without quality column)
CREATE OR REPLACE FUNCTION get_updated_channels() RETURNS TABLE (network text, station text, location text, channel text, starttime timestamp, endtime timestamp) AS $$
    SELECT DISTINCT network,station,location,channel,starttime,endtime FROM updated_channels ORDER BY network,station,location,channel;
$$ LANGUAGE SQL;

-- all updated channels (without quality and without timestamps) 
CREATE OR REPLACE FUNCTION get_updated_channels_without_timestamps() RETURNS TABLE (network text, station text, location text, channel text) AS $$
    SELECT DISTINCT network,station,location,channel FROM updated_channels ORDER BY network,station,location,channel; 
$$ LANGUAGE SQL;

-- reset updated channels
CREATE OR REPLACE FUNCTION reset_updated_channels() RETURNS VOID AS $$
    UPDATE traces SET updated=FALSE WHERE updated=TRUE;
    REFRESH MATERIALIZED VIEW updated_channels;
$$ LANGUAGE SQL;

-- force channels update
CREATE OR REPLACE FUNCTION force_channels_update() RETURNS VOID AS $$
    UPDATE traces SET updated=TRUE WHERE updated=FALSE;
    REFRESH MATERIALIZED VIEW updated_channels;
$$ LANGUAGE SQL;

  
-- Returns traces according to search parameters. 
-- Returns a server-side cursor : caller then uses SQL 'fetch' method on 
-- the cursor to get traces one by one. After usage, caller should use the 
-- 'close' SQL method to release ressource. 
-- http://www.postgresql.org/docs/9.3/static/plpgsql-cursors.html
-- FIXME quel est l'impact des LIKE en terme de perf ?
CREATE OR REPLACE FUNCTION open_traces ( 
  nw text, st text, loc text, chn text, stime timestamp, etime timestamp, samp real, qual char(1), recordlength integer) 
  RETURNS refcursor AS $$
  DECLARE
  myquery text;
  suffix1 text DEFAULT '%';
  suffix2 text DEFAULT '%';
  suffix3 text DEFAULT '%';
  suffix4 text DEFAULT '%';
  suffix4b text DEFAULT '%';  
  suffix5  text DEFAULT '';
  suffix6  text DEFAULT '';
  suffix7 text DEFAULT '';
  stimeQuoted text DEFAULT '';
  etimeQuoted text DEFAULT '';
  mycursor refcursor;
  BEGIN
    IF nw IS NOT NULL THEN suffix1 := nw; END IF;
    IF st IS NOT NULL THEN suffix2 := st; END IF;
    IF loc IS NOT NULL THEN suffix3 := loc; END IF;
    IF chn IS NOT NULL THEN suffix4 := chn; END IF;
    IF qual IS NOT NULL THEN suffix4b := qual; END IF;
    IF samp IS NOT NULL THEN suffix6 := ' AND samplingrate=' || samp ; END IF;
    IF recordlength IS NOT NULL THEN suffix7 := ' AND recordlength=' || recordlength ; END IF;

    IF stime IS NOT NULL AND etime IS NULL THEN suffix5 := ' AND endtime>=' || '''' || stime || ''' ';
    ELSIF stime IS NULL AND etime IS NOT NULL THEN suffix5 := ' AND starttime<=' || '''' || etime || ''' ';
    ELSIF stime IS NOT NULL AND etime IS NOT NULL THEN 
      -- La fonction SQL 'overlap' ne se comporte pas exactement comme attendu : a creuser.
      -- suffix5 := ' AND (SELECT (starttime,endtime) OVERLAPS (DATE ' || '''' || stime || '''' || ', ' || 'DATE ''' || etime || '''' || '))';
      -- Une expression ad-hoc est utilisee. voir:
      -- http://c2.com/cgi/wiki?TestIfDateRangesOverlap
      -- http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
      stimeQuoted := '''' || stime || '''';
      etimeQuoted := '''' || etime || '''';
      suffix5 := ' AND ( ' || stimeQuoted || ' <= endtime AND ' || etimeQuoted || ' >= starttime )';
      END IF;

    myquery := 'SELECT files.fileindex,traces.traceindex,dirname,basename,filesize,datemodified,lastupdated,network,station,location,channel,quality,samplerate,
    recordlength,fileoffset,nsamples,starttime,endtime FROM traces,files WHERE traces.fileindex=files.fileindex' || 
      ' AND network LIKE ''' || suffix1 ||
      ''' AND station LIKE ''' || suffix2 || 
      ''' AND location LIKE ''' || suffix3 || 
      ''' AND channel LIKE ''' || suffix4 || 
      ''' AND quality LIKE ''' || suffix4b || '''' || suffix5 || suffix6 || suffix7 ||
      ' ORDER BY network,station,location,channel,starttime,endtime,quality,recordlength';
    OPEN mycursor NO SCROLL FOR EXECUTE myquery;
    RETURN mycursor;
  END;
$$ LANGUAGE plpgsql;

-- Returns gaps/overlaps for a particular network/station/loc/channel
-- This function will query the gaps table 
--
-- Optional parameters :
-- network, station, location, channel (NULL means "any")
-- minratio, maxratio : select gaps whose ratiodelta is between these ranges (NULL means "any")
-- stime, etime :select gaps within this time frame (NULL means "any")
-- quality : select gaps with this quality obly (NULL means "any")

CREATE OR REPLACE FUNCTION get_gaps ( 
      nw text, stn text, loc text, chn text, 
      minratio real, maxratio real, stime timestamp, etime timestamp, quality char(1) ) RETURNS SETOF gaps AS $$
  DECLARE
    n text DEFAULT '%';
    s text DEFAULT '%';
    l text DEFAULT '%';
    c text DEFAULT '%';
    q char DEFAULT '%';
    min real;
    max real;
    t1 timestamp;
    t2 timestamp;
    -- g gaps%rowtype;
    -- selected BOOLEAN := FALSE;
    myquery text;
  BEGIN
    IF nw IS NOT NULL THEN n := nw; END IF;
    IF stn IS NOT NULL THEN s := stn; END IF;
    IF loc IS NOT NULL THEN l := loc; END IF;
    IF chn IS NOT NULL THEN c := chn; END IF;
    IF quality IS NOT NULL THEN q := quality; END IF;
    --IF minratio IS NOT NULL THEN min:=minratio; END IF;
    --IF maxratio IS NOT NULL THEN max:=maxratio; END IF;
    IF stime IS NULL THEN t1:='-infinity'; ELSE t1:=stime; END IF;
    IF etime IS NULL THEN t2:='infinity'; ELSE t2:=etime; END IF;
    IF minratio IS NULL THEN min:='-infinity'; ELSE min:=minratio; END IF;
    IF maxratio IS NULL THEN max:='+infinity'; ELSE max:=maxratio; END IF;
    
    -- http://www.postgresql.org/docs/9.3/static/plpgsql-control-structures.html
    myquery := 'SELECT * FROM gaps WHERE' ||
        ' network LIKE ' || quote_literal(n) || ' AND station LIKE ' || quote_literal(s) || 
	    ' AND location LIKE ' || quote_literal(l) || ' AND channel LIKE ' || quote_literal(c) ||
	    ' AND ratiodelta >= ' || quote_literal(min) || ' AND ratiodelta <= ' || quote_literal(max) || 
	    ' AND previous_quality LIKE ' || quote_literal(q) || ' AND current_quality LIKE ' || quote_literal(q) ||
	    ' AND ( SELECT (' || quote_literal(t1) || ',' || quote_literal(t2) || ')' || 
	    ' OVERLAPS (previous_endtime,current_starttime) ) ' ||
	    ' ORDER BY previous_endtime, current_starttime'; 
	RETURN QUERY EXECUTE myquery;
    RETURN;
    
  END;
$$ LANGUAGE plpgsql;

--
-- Returns data availability for a particular network/station/loc/channel
--
-- Optional parameters :
-- network, station, location, channel (NULL means "any")
-- stime, etime :select dataavailability within this time frame (NULL means "any")
-- gap : switch for selecting gaps insteads of data availability (== data non-availability)

CREATE OR REPLACE FUNCTION get_dataavailability ( 
      nw text, stn text, loc text, chn text, stime timestamp, etime timestamp, gap boolean) RETURNS  TABLE (network text, station text, location text, channel text, starttime timestamp, endtime timestamp) AS $$
  DECLARE
    n text DEFAULT '%';
    s text DEFAULT '%';
    l text DEFAULT '%';
    c text DEFAULT '%';
    t1 timestamp;
    t2 timestamp;
    mytype text DEFAULT 'data';
    myquery text;
  BEGIN
    IF nw IS NOT NULL THEN n := nw; END IF;
    IF stn IS NOT NULL THEN s := stn; END IF;
    IF loc IS NOT NULL THEN l := loc; END IF;
    IF chn IS NOT NULL THEN c := chn; END IF; 
    IF stime IS NULL THEN t1:='-infinity'; ELSE t1:=stime; END IF;
    IF etime IS NULL THEN t2:='infinity'; ELSE t2:=etime; END IF;
    IF gap THEN mytype:='gap'; END IF;
    
    myquery := 'SELECT network,station,location,channel,starttime,endtime FROM dataavailability WHERE' || ' network LIKE ' || quote_literal(n) || ' AND station LIKE ' || quote_literal(s) || ' AND location LIKE ' || quote_literal(l) || ' AND channel LIKE ' || quote_literal(c) || ' AND ( SELECT (' || quote_literal(t1) || ',' || quote_literal(t2) || ')' || ' OVERLAPS (starttime,endtime) ) ' || ' AND type='|| quote_literal(mytype) || ' ORDER BY network,station,location,channel,starttime,endtime'; 
	RETURN QUERY EXECUTE myquery;
    RETURN;  
  END;
$$ LANGUAGE plpgsql;


--
-- get earliest startime for a channel's traces.
--
-- if stime is NULL : returns the starttime of the earliest trace 
-- if stime is not NULL :
--      if stime is included in one of the traces timerange (stime in [starttime;endtime]) : returns stime
--         else returns next available trace's starttime 
--          if no next trace available, returns NULL
CREATE OR REPLACE FUNCTION get_channel_mintime ( nw text, stn text, loc text, chn text, stime timestamp ) RETURNS timestamp AS $$
  DECLARE
  tresult timestamp DEFAULT NULL;
  BEGIN
    -- if stime is not specified, return the earliest starttime  
    IF stime IS NULL THEN 
        WITH mytable AS (SELECT starttime FROM traces WHERE network=nw AND station=stn AND location=loc AND channel=chn) SELECT min(starttime) FROM mytable INTO tresult;
    -- if stime is specified,   
    ELSE
        -- if included in the timerange of a trace => returns stime
        IF EXISTS ( SELECT true FROM traces WHERE stime >=starttime AND stime <= endtime AND network=nw AND station=stn AND location=loc AND channel=chn )
        THEN
            tresult:=stime;
        -- if not included, returns next available starttime
        ELSE
            WITH mytable AS (SELECT starttime FROM traces  WHERE network=nw AND station=stn AND location=loc AND channel=chn AND starttime>=stime) SELECT min(starttime) FROM mytable INTO tresult;
        END IF;
    END IF;
  RETURN tresult;
  END;
$$ LANGUAGE plpgsql;

--
-- get latest endtime for a channel's traces.
--
-- if etime is NULL : returns the endtime of the latest trace 
-- if etime is not NULL :
--      if etime is included in one of the traces timerange (etime in [starttime;endtime]) : returns etime
--         else returns last available trace's endtime 
--          if no last trace available, returns NULL
CREATE OR REPLACE FUNCTION get_channel_maxtime ( nw text, stn text, loc text, chn text, etime timestamp ) RETURNS timestamp AS $$
  DECLARE
  tresult timestamp DEFAULT NULL;
  BEGIN
    -- if stime is not specified, return the earliest starttime  
    IF etime IS NULL THEN 
        WITH mytable AS (SELECT endtime FROM traces WHERE network=nw AND station=stn AND location=loc AND channel=chn) SELECT max(endtime) FROM mytable INTO tresult;
    -- if stime is specified,   
    ELSE
        -- if included in the timerange of a trace => returns etime
        IF EXISTS ( SELECT true FROM traces WHERE etime >=starttime AND etime <= endtime AND network=nw AND station=stn AND location=loc AND channel=chn)
        THEN
            tresult:=etime;
        -- if not included, returns last available endtime
        ELSE
            WITH mytable AS (SELECT endtime FROM traces  WHERE network=nw AND station=stn AND location=loc AND channel=chn AND endtime<=etime) SELECT max(endtime) FROM mytable INTO tresult;
        END IF;
    END IF;
  RETURN tresult;
  END;
$$ LANGUAGE plpgsql;


