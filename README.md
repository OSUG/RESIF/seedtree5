
Seedtree is a tool for indexing the characteristics of [miniseed](http://ds.iris.edu/ds/nodes/dmc/data/formats/miniseed/) files. Seedtree runs through a list of files, collects in a database the metadata of the miniseed files it found. As a result of this scan, the Seedtree API can be used by third-party codes to query the database. Seedtree runs on top of Filetree, a script for indexing POSIX file attributes, included in this repository.

[Installation instructions and user manual](https://github.com/resif/seedtree5/blob/master/USER_MANUAL.md)

----------

Seedtree was originally developped for [RESIF datacenter](http://seismology.resif.fr) internal needs. You may also be interested by [EIDA WFcatalog](https://github.com/EIDA/wfcatalog) or [IRIS mseedindex](https://github.com/iris-edu/mseedindex).

